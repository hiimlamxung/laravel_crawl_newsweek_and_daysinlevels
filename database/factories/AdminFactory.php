<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admins\Admin;
use Faker\Generator as Faker;

$factory->define(Admin::class, function (Faker $faker) {
    return [
        'username' => 'admin',
        'email'    => 'admin@gmail.com',
        'password' => bcrypt('123456'),
        'active'   => true,
        'role'     => true,
    ];
});
