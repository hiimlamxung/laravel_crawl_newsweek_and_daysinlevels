<?php
use App\Models\Admins\Admin;
use App\Models\Admins\Role;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(LaratrustSeeder::class);
        factory(Admin::class)->create();

        // setup role administrator to admin
        $admin = Admin::first();
        $role = Role::whereName('administrator')->first();
        $admin->attachRole($role);
        $admin->syncPermissions($admin->getPermissionsViaRoles());
    }
}
