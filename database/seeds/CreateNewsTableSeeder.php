<?php

use Illuminate\Database\Seeder;
use App\Models\News;

class CreateNewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        for ($i=0; $i < 20; $i++) { 
            News::insert([
                		'user_id'		=>	rand(1,100),
                		'pub_date'		=>	$faker->datetime,
                		'title'	=>	$faker->name,
                		'link'	=>	$faker->name,
                		'video'	=>	$faker->name,
                		'image'	=>	$faker->name,
                		'user_post'	=>	rand(1,100),
                		'description'	=>	$faker->name,
                		'content'	=>	$faker->name,
                        'status'    =>  mt_rand(-1,4)
                	]);
        }
    }
}
