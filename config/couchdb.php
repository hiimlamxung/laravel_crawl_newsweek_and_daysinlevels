<?php

return [
    'host'      => env('COUCHDB_HOST'),
    'port'      => env('COUCHDB_PORT'),
    'dbname'    => env('COUCHDB_DATABASE'),
    'username'  => env('COUCHDB_USERNAME'),
    'password'  => env('COUCHDB_PASSWORD')

];