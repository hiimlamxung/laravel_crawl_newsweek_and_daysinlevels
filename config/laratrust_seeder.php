<?php

return [
    'role_structure' => [
        'administrator' => [
            'news' => 'c,r,u,d,a,ds',
            'statistical' => 'r',
            'profile' => 'r,u',
            'admins' => 'c,r,u,d',
            'roles' => 'c,r,u,d',
            
        ],
        'censorship' => [
            'profile' => 'r,u',
            'news' => 'r,u,d,a,ds',
        ],
        'editor' => [
            'profile' => 'r,u',
            'news' => 'c,r,u,d',
        ],
    ],
    'permission_structure' => [
        // 'cru_user' => [
        //     'profile' => 'r,u'
        // ]
    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete',
        'a' => 'action',
        'ds' => 'delete list'
    ]
];
