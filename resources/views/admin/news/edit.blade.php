@extends('layout.admin')
@section('title')
    <title> Sửa bài báo </title>
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="h3-title">Cập nhật bài báo:</h3><br>
                        <form  method="POST" enctype="multipart/form-data" id="frm_edit_news">
                            @csrf
                            <div class="col-md-12">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        @foreach ($errors->all() as $er)
                                            {{ $er }} <br>
                                        @endforeach
                                    </div>
                                @endif
                                @if (session('thongbao'))
                                    <div class="alert alert-success">
                                        {{ session('thongbao') }}
                                    </div>
                                @endif
                                @csrf
                                <div class="form-group">
                                    <input type="hidden" value="{{ $new->id }}" name="id" id="news_id">
                                    <label for="">Tiêu đề:</label>
                                    <input type="text" class="form-control" id="title" name="title"
                                        placeholder="Nhập tiêu đề ..." value="{{ $new->title }}">
                                </div>
                                <div class="form-group">
                                    <label for="">Link:</label>
                                    <input type="text" class="form-control" id="link" name="link"
                                        placeholder="Nhập link ..." value="{{ $new->link }}">
                                </div>
                                <div class="form-group">
                                    <label for="">Public date:</label>
                                    <input type="datetime-local" class="form-control" name="pub_date" id="pub_date"
                                        value="{{ str_replace(' ', 'T', $new->pub_date) }}">
                                </div>
                                <div class="form-group">
                                    <label for="">Ảnh đại diện</label>
                                    <img src="{{ $new->image }}" alt="" width="100">
                                    <input type="file" class="form-control-file" name="image" id="image">
                                </div>
                                <div class="form-group">
                                    <label for="">Ảnh chi tiết:</label><br>
                                    <div class="row">
                                        @foreach ($new->ListImgNews as $img)
                                            <div class="col-md-2 box_img_news" data-id="{{ $img->id }}">
                                                <button class="btn btn-danger btn_del_listimg"
                                                    data-id="{{ $img->id }}">
                                                    Xoá</button>
                                                <img src="{{ $img->path }}" alt="" class="list_img_news">
                                            </div>

                                        @endforeach
                                    </div>
                                    <input type="file" multiple="" class="form-control-file" name="list_img[]"
                                        id="list_img">
                                </div>
                                <div class="form-group">
                                    <label for="">Video:</label><br>
                                    @if ($new->video)
                                        <div class="box-video">
                                            <video controls preload>
                                                <source src="{{ $new->video }}">
                                            </video>
                                            <a href="" class="del_video_new" data-id="{{ $new->id }}">Xoá</a>
                                        </div>
                                    @endif
                                    <input type="file" class="form-control-file" name="video" id="video">
                                </div>
                                <div class="form-group">
                                    <label for="danhmuc">Status:</label>
                                    <select class="form-control" name="status" id="status">
                                        <option value="0" @if ($new->status == 0) {{ 'selected' }} @endif>Mới thêm</option>
                                        <option value="1" @if ($new->status == 1) {{ 'selected' }} @endif>Chờ phát hành</option>
                                        <option value="2" @if ($new->status == 2) {{ 'selected' }} @endif>Đã phát hành</option>
                                        <option value="-1" @if ($new->status == -1) {{ 'selected' }} @endif>Tạm ngừng</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Nhập mô tả:</label>
                                    <textarea name="description" class="form-control ckeditor" id="description" cols="20"
                                        rows="5">{{ $new->description }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Nhập nội dung:</label>
                                    <textarea name="content" class="form-control ckeditor" id="content" cols="30"
                                        rows="10">{{ $new->content }}</textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Cập nhật</button>
                        </form>
                    </div>


                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
    </div>
@endsection
@include('partials.paramsJS')
@section('script')
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
    <script src="{{asset('backend/js/news/edit.news.js')}}"></script>
@endsection
