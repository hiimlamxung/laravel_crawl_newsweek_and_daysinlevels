@extends('layout.admin')

@section('title')
    <title>Bài báo</title>
@endsection
@section('css')
<style>
    #frm_filter_news .form-group:not(#frm_filter_news .form-group:nth-of-type(1)) {
        margin-left:20px;
    }

</style>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="page-title">
                        <div class="title_left">
                            <h3 class="h3-title">Danh mục bài báo:</h3>
                        </div><br>
                        <div class="title_rightw">
                            <div class="col-md-12 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <form action="{{ route('news.manager') }}" method="GET" class="row" id="frm_filter_news">
                                    @csrf
                                    <div class="form-group">
                                        <input type="text" name="search" class="form-control input-search"
                                            placeholder="Tìm kiếm bài báo" value="{{ Request::get('search') }}">
                                        <small class="text-muted">Tiêu đề</small>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control fillter_width_url" name="status">
                                            <option {{ Request::get('status') == 'all' ? 'selected' : '' }} value="all">
                                                Tất cả</option>
                                            <option {{ Request::get('status') == 'new' ? 'selected' : '' }} value="new">
                                                Mới thêm</option>
                                            <option {{ Request::get('status') == 'posted' ? 'selected' : '' }}
                                                value="posted">Chờ phát hành</option>
                                            <option {{ Request::get('status') == 'success' ? 'selected' : '' }}
                                                value="success">Đã phát hành</option>
                                            <option {{ Request::get('status') == 'deleted' ? 'selected' : '' }}
                                                value="deleted">Tạm ngừng</option>
                                        </select>
                                        <small class="text-muted">Trạng thái</small>
                                    </div>
                                    <div class="form-group">
                                        <input type="date" name="pub_date" class="form-control"
                                            value="{{ Request::get('pub_date') }}">
                                        <small class="text-muted">Ngày</small>
                                    </div>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-search" type="submit">Lọc</button>
                                    </span>
                                </form>
                            </div>
                            @if ($guard->can('create-news'))
                                <a href="{{ route('news.create') }}"><button type="button"
                                        class="btn btn-sm btn-success">Thêm mới
                                        bài báo</button></a>
                            @endif


                            @if ($guard->can('action-news'))
                                <button type="button" class="btn btn-sm btn-info" id="post_news">Phát hành báo</button>
                            @endif
                            @if ($guard->can('delete-list-news'))
                                <button type="button" class="btn btn-sm btn-danger" id="del_list_news">Xóa</button>
                            @endif
                        </div>
                    </div>
                </div>
                @if (session('thongbao'))
                    <script>
                        alert('{{ session('thongbao') }}');

                    </script>
                @endif
                <div id="boxout_table_manager_news">
                    <div id="boxin_table_manager_news">
                        @if (count($news) > 0)
                            <div class="row">
                                <table class="table table-hover" id="table_manager_news">
                                    <thead>
                                        <tr>
                                            <th scope="col"><input type="checkbox" id="check_all_news"></th>
                                            <th scope="col">Ảnh</th>
                                            <th scope="col">Tiêu đề</th>
                                            <th scope="col" style="width: 80px;">Nội dung</th>
                                            <th scope="col">Người viết</th>
                                            <th scope="col">Public date</th>
                                            <th scope="col" style="width: 170px;">Status</th>
                                            <th scope="col" style="width:250px;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($news as $new)
                                            <tr>
                                                <td><input type="checkbox" data-id="{{ $new->id }}"
                                                        class="check_news">
                                                </td>
                                                <td><img style="width:100px;" src="{{ $new->image }}" alt=""></td>
                                                <td>{{ $new->title }}</td>
                                                <td><a href="javascript:void(0)" class="detail-cont"
                                                        data-content="{{ $new->content }}">Chi tiết</a></td>
                                                <td>{{ $new->admins->username }}</td>
                                                <td>{{ $new->pub_date }}</td>


                                                @if ($guard->can('action-news') && $new->status != 2)
                                                    <td>
                                                        <select name="" class="form-control stt_of_news"
                                                            data-id="{{ $new->id }}">
                                                            <option value="0" @if ($new->status == 0) {{ 'selected' }} @endif>
                                                                Mới thêm
                                                            </option>
                                                            <option value="1" @if ($new->status == 1) {{ 'selected' }} @endif>
                                                                Chờ phát hành
                                                            </option>
                                                            <option value="2" @if ($new->status == 2) {{ 'selected' }} @endif>
                                                                Đã phát hành
                                                            </option>
                                                            <option value="-1" @if ($new->status == -1) {{ 'selected' }} @endif>
                                                                Tạm ngừng
                                                            </option>
                                                        </select>
                                                    </td>
                                                @elseif($guard->can('action-news') && $new->status == 2)
                                                    <td><button class="btn btn-sm btn-success">Đã được phát hành</button>
                                                    </td>
                                                @else
                                                    <td>
                                                        @switch($new->status)
                                                            @case(0)
                                                                <button class="btn btn-sm btn-secondary">Mới thêm</button>
                                                                @break
                                                            @case(1)
                                                                <button class="btn btn-sm btn-primary">Chờ phát hành</button>
                                                                @break
                                                            @case(2)
                                                                <button class="btn btn-sm btn-success">Đã được phát hành</button>
                                                                @break
                                                            @case(-1)
                                                                <button class="btn btn-sm btn-danger">Tạm ngừng</button>
                                                                @break
                                                        @endswitch
                                                    </td>
                                                @endif
                                                <td>
                                                    @if ($new->status != 2)
                                                        @if (($guard->can('update-news') && $guard->id == $new->user_id) || ($guard->can('update-news') && $guard->hasRole('administrator')))
                                                            <a href="{{ route('news.edit', ['id' => $new->id]) }}"><button
                                                                    type="button"
                                                                    class="btn btn-sm btn-primary">Sửa</button></a>
                                                        @endif

                                                        @if (($guard->can('delete-news') && $guard->id == $new->user_id) || ($guard->can('delete-news') && $guard->hasRole('administrator')))
                                                            <a href="{{ route('news.delete', ['id' => $new->id]) }}"><button
                                                                    type="button"
                                                                    class="btn btn-danger btn-sm btn_del_new">Xóa</button></a>
                                                        @endif
                                                    @endif

                                                    @if ($guard->can('action-news') && $new->status != 2)
                                                        @if (in_array($new->status, [0, -1]))
                                                            <button type="button"
                                                                class="btn_update_stt btn-sm btn btn-success" data-stt="1"
                                                                data-id="{{ $new->id }}">Phát
                                                                hành</button>
                                                        @else
                                                            <button type="button"
                                                                class="btn_update_stt btn btn-sm btn-secondary"
                                                                data-stt="-1" data-id="{{ $new->id }}">Tạm
                                                                ngừng</button>
                                                        @endif
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row d-flex justify-content-center">
                                {{ $news->links() }}
                            </div>
                        @else
                            <h5 style="color: gray;">Không có kết quả</h5>
                        @endif
                    </div>
                </div>

                {{-- modal show content --}}
                <div class="modal fade" tabindex="-1" role="dialog" id="modal-detail">
                    <div class="modal-dialog custom-modal-detail-news modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body text-news"></div>
                        </div>
                    </div>
                </div>

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@include('partials.paramsJS')
@section('script')
    <script src="{{ asset('backend/js/news/manager.news.js') }}"></script>
@endsection
