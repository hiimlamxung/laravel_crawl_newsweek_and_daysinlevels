@extends('layout.admin')
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="h3-title">Thêm mới bài báo:</h3><br>
                        <form action="" method="POST" enctype="multipart/form-data" id="frm_add_news">
                            @csrf
                            <div class="col-md-12">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        @foreach ($errors->all() as $er)
                                            {{ $er }} <br>
                                        @endforeach
                                    </div>
                                @endif
                                @if (session('thongbao'))
                                    <div class="alert alert-success">
                                        {{ session('thongbao') }}
                                    </div>
                                @endif
                                @csrf
                                <div class="form-group">
                                    <label for="">Tiêu đề:</label>
                                    <input type="text" class="form-control" id="title" name="title"
                                        placeholder="Nhập tiêu đề ...">
                                </div>
                                <div class="form-group">
                                    <label for="">Link:</label>
                                    <input type="text" class="form-control" id="link" name="link"
                                        placeholder="Nhập link ...">
                                </div>
                                <div class="form-group">
                                    <label for="">Public date:</label>
                                    <input type="datetime-local" class="form-control" name="pub_date" id="pub_date">
                                </div>
                                <div class="form-group">
                                    <label for="">Ảnh đại diện</label>
                                    <input type="file" class="form-control-file" name="image" id="image">
                                </div>
                                <div class="form-group">
                                    <label for="">Ảnh chi tiết:</label>
                                    <input type="file" multiple="" class="form-control-file" name="list_img[]"
                                        id="list_img">
                                </div>
                                <div class="form-group">
                                    <label for="">Video:</label>
                                    <input type="file" class="form-control-file" name="video" id="video">
                                </div>
                                {{-- <div class="form-group">
                                    <label for="danhmuc">Status:</label>
                                    <select class="form-control" name="status" id="status">
                                        <option value="0">Mới thêm</option>
                                        <option value="1">Chờ phát hành</option>
                                        <option value="2">Đã phát hành</option>
                                        <option value="-1">Tạm ngừng</option>
                                    </select>
                                </div> --}}
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Nhập mô tả:</label>
                                    <textarea name="description" class="form-control ckeditor" id="description" cols="20"
                                        rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Nhập nội dung:</label>
                                    <textarea name="content" class="form-control ckeditor" id="content" cols="30"
                                        rows="10"></textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>


                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
    </div>
@endsection
@include('partials.paramsJS')
@section('script')
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
    <script src="{{ asset('backend/js/news/create.news.js') }}"></script>

@endsection
