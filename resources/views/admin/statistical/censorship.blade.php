@extends('layout.admin')

@section('title')
    <title>Báo cáo, thống kê</title>
@endsection
@section('css')
<style>
    .link-decoration:hover{
        text-decoration: underline;
    }
    #frm_filter_statiscical .form-group:not(#frm_filter_statiscical .form-group:nth-of-type(1)) {
        margin-left:20px;
    }
</style>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="title_left">
                    <h3 class="h3-title">Báo cáo, thống kê:</h3>
                </div><br><hr>
                <div class="row d-flex justify-content-center">
                <ul class="nav nav-pills">
                    <li class="nav-item">
                        <a  class="link-decoration nav-link @if (Request::is('backend/statistical/ctv'))
                        active
                    @endif" href="{{ route('admin.statistical.ctv') }}">Cộng tác viên</a>
                    </li>
                    <li class="nav-item">
                        <a class="link-decoration nav-link @if (Request::is('backend/statistical/censorship'))
                        active
                    @endif" href="{{ route('admin.statistical.censorship') }}">Kiểm duyệt viên</a>
                    </li>
                    </li>
                </ul></div><hr>
                <div class="title_rightw">
                    <div class="col-md-12 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <form action="" method="GET" class="row" id="frm_filter_statiscical">
                            @csrf
                            <div class="form-group">
                                <select name="month" id="month" class="form-control">
                                    @for ($i = 1; $i <= 12; $i++)
                                        <option value="{{ $i }}" @if ($i == $month) {{ 'selected' }} @endif>Tháng {{ $i }}
                                        </option>
                                    @endfor
                                </select>
                                <small class="text-muted">Tháng:</small>
                            </div>
                            <div class="form-group">
                                <select name="year" id="year" class="form-control">
                                    @for ($i = 2021; $i <= 2030; $i++)
                                        <option value="{{ $i }}" @if ($i == $year) {{ 'selected' }} @endif>Năm {{ $i }}
                                        </option>
                                    @endfor
                                </select>
                                <small class="text-muted">Năm:</small>
                            </div>
                            <div class="form-group">
                                <select name="id" id="id" class="form-control">
                                    <option value="all">Tất cả</option>
                                    @foreach ($list_censorship as $admin)
                                        <option value="{{ $admin->id }}" @if ($admin->id == $id) {{ 'selected' }} @endif>{{ $admin->username }}
                                        </option>
                                    @endforeach
                                </select>
                                <small class="text-muted">Cộng tác viên:</small>
                            </div>
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-search" type="submit">Lọc</button>
                            </span>
                        </form>
                    </div>
                </div>
                <div class="box-header with-border">
                    <div class="col-md-2">
                        <p>Tổng số bài báo đã viết: <strong>{{ $count['news'] }}</strong></p>
                    </div>
                    <div class="col-md-3">
                        <p>Số bài đã phát hành: <strong>{{ $count['success'] }}</strong></p>
                    </div>
                    <div class="col-md-3">
                        <p>Số bài chờ phát hành: <strong>{{ $count['publish'] }}</strong></p>
                    </div>
                    <div class="col-md-2">
                        <p>Số bài bị xoá: <strong style="color:red">{{ $count['deleted'] }}</strong></p>
                    </div>
                </div>
                <div id="boxout_table_statistical">
                    <div id="boxin_table_statistical">
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover tbl-vertical-middle"
                                style="text-align: center;">
                                <thead>
                                    <tr>
                                        <th scope="col">STT</th>
                                        <th scope="col">Người duyệt</th>
                                        <th scope="col">Tiêu đề</th>
                                        <th scope="col">Ngày đăng</th>
                                        <th scope="col">Trạng thái</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $stt = 0; ?>
                                    @foreach ($statistical_pagi as $key => $new)
                                        <tr>
                                            <td>{{ $key + $statistical_pagi->firstItem() }}</td>
                                            <td>{{ $new->adminAction->username }}</td>
                                            <td>{{ $new->title }}</td>
                                            <td>{{ $new->pub_date }}</td>
                                            <td>
                                                @if ($new->status == 0)
                                                    <button class="btn btn-sm btn-secondary">Mới thêm</button>
                                                @elseif ($new->status == 1)
                                                    <button class="btn btn-sm btn-info">Chờ phát hành</button>
                                                @elseif ($new->status == 2)
                                                    <button class="btn btn-sm btn-success">Đã phát hành</button>
                                                @else
                                                    <button class="btn btn-sm btn-danger">Tạm ngừng</button>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                        </div>

                        <div class="row d-flex justify-content-center">
                            {{ $statistical_pagi->links() }}
                        </div>
                    </div>
                </div>

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('script')
    
@endsection
