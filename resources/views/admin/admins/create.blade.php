@extends('layout.admin')
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('admin.admins.store') }}" method="POST" enctype="multipart/form-data" id="frm_add_admin">
                            @csrf
                            <div class="col-md-12">
                                <h3 class="h3-title">Thêm mới thành viên:</h3><br>
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        {{ $errors->first() }}
                                    </div>
                                @endif
                                @if (session('thongbao'))
                                    <div class="alert alert-success">
                                        {{ session('thongbao') }}
                                    </div>
                                @endif
                                @csrf
                                <div class="form-group">
                                    <label for="">Họ tên:</label>
                                    <input type="text" class="form-control" id="username" name="username"
                                        placeholder="Nhập họ tên ..." value="{{ old('username') }}">
                                </div>
                                <div class="form-group">
                                    <label for="">Email:</label>
                                    <input type="email" class="form-control" id="email" name="email"
                                        placeholder="Nhập email ..." value="{{ old('email') }}">
                                </div>
                                <div class="form-group">
                                    <label for="">Password:</label>
                                    <input type="password" class="form-control" name="password" id="password">
                                </div>
                                <div class="form-group">
                                    <label for="">Re-Password:</label>
                                    <input type="password" class="form-control" name="password_confirmation" id="password">
                                </div>
                                <div class="form-group">
                                    <label for="">Chức vụ:</label>
                                    <select name="role" class="form-control">
                                        @foreach ($roles as $role)
                                        <option value="{{$role->id}}">{{$role->display_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>


                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
    </div>
@endsection

@section('script')
    
@endsection
