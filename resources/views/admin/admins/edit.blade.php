@extends('layout.admin')
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('admin.admins.update',['id' => $admin->id]) }}" method="POST" enctype="multipart/form-data" id="frm_add_admin">
                            @csrf
                            <div class="col-md-12">
                                <h3 class="h3-title">Sửa thông tin thành viên:</h3>
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        {{ $errors->first() }}
                                    </div>
                                @endif
                                @if (session('thongbao'))
                                    <div class="alert alert-success">
                                        {{ session('thongbao') }}
                                    </div>
                                @endif
                                @csrf
                                
                                <div class="form-group">
                                    <input type="hidden" name="id" value="{{$admin->id}}">
                                    <label for="">Họ tên:</label>
                                    <input type="text" class="form-control" id="username" name="username"
                                        placeholder="Nhập họ tên ..." value="{{ $admin->username }}">
                                </div>
                                <div class="form-group">
                                    <label for="">Email:</label>
                                    <input type="email" class="form-control" id="email" name="email"
                                        placeholder="Nhập email ..." value="{{ $admin->email }}">
                                </div>
                                <div class="form-group">
                                    <label for="">Avatar:</label>
                                    <img src="{{ $admin->image }}" alt="" width="100">
                                    <input type="file" class="form-control" name="image" id="image">
                                </div>

                                <div class="form-group">
                                    <label for="">Chức vụ:</label>
                                    <select name="role" class="form-control">
                                        <option value="0" @if ($admin->role == 0) selected @endif>Cộng tác viên</option>
                                        <option value="1" @if ($admin->role == 1) selected @endif>Admin</option>
                                        <option value="2" @if ($admin->role == 2) selected @endif>Kiểm duyệt viên</option>
                                    </select>
                                </div>
                                
                            </div>
                            <button type="submit" class="btn btn-primary">Cập nhật</button>
                        </form>
                    </div>


                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
    </div>
@endsection

@section('script')
    
@endsection
