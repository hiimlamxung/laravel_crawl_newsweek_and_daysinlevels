@extends('layout.admin')

@section('title')
    <title>Phân quyền</title>
@endsection
@section('css')

@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                @if (session('thongbao'))
                    <script>
                        alert('{{ session('thongbao') }}');

                    </script>
                @endif
                <div class="row">
                    <div class="col-md-9">
                        <h3 class="h3-title">Quyền:</h3><br>
                        <div id="boxout_table_roles">
                            <div id="boxin_table_roles">
                                <div class="row">
                                    <table class="table table-hover table-striped" id="table_manager_news">
                                        <thead>
                                            <tr>
                                                <th>STT</th>
                                                <th scope="col">Role</th>
                                                <th scope="col">Description</th>
                                                <th scope="col">Permission</th>
                                                <th scope="col" style="width: 200px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($roles as $key => $role)
                                                <tr>
                                                    <td>{{ $key + $roles->firstItem() }}</td>
                                                    <td>{{ $role->display_name }}</td>
                                                    <td>{{ $role->description }}</td>
                                                    <td>
                                                        @foreach ($role->permissions as $permission)
                                                            <span class="badge badge-info">{{ $permission->display_name }}</span>
                                                        @endforeach
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-primary btn-add-per-to-role" data-id="{{$role->id}}" data-key="{{$key}}" ><i
                                                                class="fa fa-cog"></i> Gắn vai trò</button>
                                                        <button type="button"
                                                                data-id="{{$role->id}}"  class="btn btn-sm btn-danger btn-delete-role"><i
                                                                    class="fa fa-trash-o"></i> Xoá</button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="clearfix"></div>
                                </div>

                                {{-- <div class="row d-flex justify-content-center"> --}}
                                    {{-- {{ $lists->links() }} --}}
                                {{-- </div> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <form action="{{route('admin.admins.roles.storerole')}}" method="POST" style="background:#dee2e6;padding:10px;">
                            @csrf
                            <h4>Thêm quyền mới</h4>
                            <hr>
                            <div class="form-group ">
                                <label>Tên</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control" placeholder="Tên quyền" required>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="">Tên hiển thị</label>
                                <div class="col-sm-10">
                                    <input type="text" name="display_name" class="form-control" placeholder="Tên hiển thị" required>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="">Chú thích</label>
                                <div class="col-sm-10">
                                    <input type="text" name="description" class="form-control" placeholder="Chú thích" required>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Thêm</button>
                        </form>
                        <hr>
                        <form action="{{route('admin.admins.roles.storepermission')}}" method="POST" style="background:#dee2e6;padding:10px;">
                            @csrf
                            <h4>Thêm chức năng mới</h4>
                            <hr>
                            <div class="form-group ">
                                <label>Tên</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control" placeholder="Tên quyền" required>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="">Tên hiển thị</label>
                                <div class="col-sm-10">
                                    <input type="text" name="display_name" class="form-control" placeholder="Tên hiển thị" required>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="">Chú thích</label>
                                <div class="col-sm-10">
                                    <input type="text" name="description" class="form-control" placeholder="Chú thích" required>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Thêm</button>
                        </form>
                    </div>
                </div>
                {{-- Modal list permissions --}}
                <div class="modal fade bs-add-permission-role-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">

                            <div class="modal-header">
                            
                                <h4 class="modal-title" id="">Danh sách vai trò</h4>
                            </div>
                            <div class="modal-body">
                                <ul class="to_do">
                                    @foreach ($permissions as $permission)
                                        <li style="list-style: none">
                                            <p><input type="checkbox" name="{{ $permission->name }}"
                                                    value="{{ $permission->id }}"> {{ $permission->display_name }} </p>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success btn-save-change-permission">Cập nhật</button>
                            </div>

                        </div>
                    </div>
                </div>

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@include('partials.paramsJS')
<!--put param form PHP to javascript-->
@section('script')
    <script src="{{ asset('backend/js/admins/roles.admins.js') }}"></script>
@endsection
