@extends('layout.admin')

@section('title')
    <title>Quản lý thành viên</title>
@endsection
@section('css')

@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="page-title">
                        <div class="title_left">
                            <h3 class="h3-title">Quản lý thành viên:</h3>
                        </div><br>
                        <div class="title_rightw">
                            {{-- <div class="col-md-12 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <form action="" method="GET" class="row">
                                    @csrf
                                    <div class="form-group">
                                        <input type="text" name="search" class="form-control input-search"
                                            placeholder="Tìm kiếm bài báo" value="{{ Request::get('search') }}">
                                        <small id="helpId" class="text-muted">Tiêu đề</small>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control fillter_width_url" name="status">
                                            <option {{ Request::get('status') == 'all' ? 'selected' : '' }} value="all">
                                                Tất cả</option>
                                            <option {{ Request::get('status') == 'new' ? 'selected' : '' }} value="new">
                                                Mới thêm</option>
                                            <option {{ Request::get('status') == 'posted' ? 'selected' : '' }}
                                                value="posted">Chờ phát hành</option>
                                            <option {{ Request::get('status') == 'success' ? 'selected' : '' }}
                                                value="success">Đã phát hành</option>
                                            <option {{ Request::get('status') == 'deleted' ? 'selected' : '' }}
                                                value="deleted">Tạm ngừng</option>
                                        </select>
                                        <small id="helpId" class="text-muted">Trạng thái</small>
                                    </div>
                                    <div class="form-group">
                                        <input type="date" name="pub_date" class="form-control"
                                            value="{{ Request::get('pub_date') }}">
                                        <small id="helpId" class="text-muted">Ngày</small>
                                    </div>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-search" type="submit">Lọc</button>
                                    </span>
                                </form>
                            </div> --}}
                            <a href="{{ route('admin.admins.create') }}"><button type="button"
                                    class="btn btn-sm btn-success">Thêm mới
                                    thành viên</button></a>
                        </div>
                    </div>
                </div>
                @if (session('thongbao'))
                    <script>
                        alert('{{ session('thongbao') }}');

                    </script>
                @endif
                <div id="boxout_table_manager_news">
                    <div id="boxin_table_manager_news">
                        @if (count($lists) > 0)
                            <div class="row">
                                <table class="table table-hover" id="table_manager_news">
                                    <thead>
                                        <tr>
                                            <th scope="col">STT</th>
                                            <th scope="col">Tên</th>
                                            <th scope="col">Ảnh</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Chức vụ</th>
                                            <th scope="col">Trạng thái</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($lists as $key => $list)
                                            <tr>
                                                <td>{{ $key + $lists->firstItem() }}</td>
                                                <td>{{ $list->username }}</td>
                                                <td>
                                                    <img style="width:100px;"
                                                        src="{{ $list->image ? $list->image : asset('upload/images/user.png') }}"
                                                        alt="">
                                                </td>
                                                <td>{{ $list->email }}</td>
                                                <td>
                                                    @foreach ($list->roles as $role)
                                                        <span class="badge badge-info">{{ $role->display_name }}</span>
                                                    @endforeach
                                                </td>
                                                <td>
                                                    @if ($list->active == 1)
                                                        <button type="button" class="btn btn-sm btn-success btn_active"
                                                            data-active="0" data-id="{{ $list->id }}">Hoạt động</button>
                                                    @else
                                                        <button type="button" class="btn btn-sm btn-danger btn_active"
                                                            data-active="1" data-id="{{ $list->id }}">Ngừng hoạt
                                                            động</button>
                                                    @endif
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-sm btn-dark btn-role-for-admin"
                                                        data-key="{{ $key }}"
                                                        data-id="{{ $list->id }}">Role</button>
                                                    <button type="button"
                                                        class="btn btn-sm btn-info btn-permission-for-admin"
                                                        data-key="{{ $key }}" data-id="{{ $list->id }}"><i
                                                            class="fa fa-cog"></i> Permission</button>
                                                    <a href="{{ route('admin.admins.edit', ['id' => $list->id]) }}"><button
                                                            type="button" class="btn btn-sm btn-primary">Sửa</button></a>
                                                    <a href="{{ route('admin.admins.delete', ['id' => $list->id]) }}"><button
                                                            type="button"
                                                            class="btn btn-danger btn-sm btn_del_admin">Xoá</button></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row d-flex justify-content-center">
                                {{ $lists->links() }}
                            </div>
                        @else
                            <h5 style="color: gray;">Không có kết quả</h5>
                        @endif
                    </div>
                </div>

                {{-- modal --}}
                <div class="modal fade" tabindex="-1" role="dialog" id="modal-detail">
                    <div class="modal-dialog custom-modal-detail-news modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body text-news"></div>
                        </div>
                    </div>
                </div>

                {{-- Modal list roles --}}
                <div class="modal fade modal-role-admin" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">

                            <div class="modal-header">
                                <h4 class="modal-title" id="">Danh sách quyền</h4>
                            </div>
                            <div class="modal-body">
                                <ul class="to_do">
                                    @foreach ($roles as $role)
                                        <li style="list-style: none">
                                            <p><input type="checkbox" class="roles" name="{{ $role->name }}"
                                                    value="{{ $role->id }}"> {{ $role->display_name }} </p>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success btn-save-change-role-admin">Cập nhật</button>
                            </div>

                        </div>
                    </div>
                </div>

                {{-- Modal list permissions --}}
                <div class="modal fade modal-permission-admin" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">

                            <div class="modal-header">
                                
                                <h4 class="modal-title" id="">Danh sách vai trò</h4>
                            </div>
                            <div class="modal-body">
                                <ul class="to_do">
                                    @foreach ($permissions as $permission)
                                        <li style="list-style: none">
                                            <p><input type="checkbox" class="permissions" name="{{ $permission->name }}"
                                                    value="{{ $permission->id }}"> {{ $permission->display_name }}
                                            </p>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success btn-save-change-permission">Cập nhật</button>
                            </div>

                        </div>
                    </div>
                </div>

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@include('partials.paramsJS')
<!--put param form PHP to javascript-->
@section('script')
    <script src="{{ asset('backend/js/admins/index.admins.js') }}"></script>
@endsection
