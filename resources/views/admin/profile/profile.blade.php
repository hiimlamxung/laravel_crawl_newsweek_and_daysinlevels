@extends('layout.admin')

@section('title')
    <title>Thông tin cá nhân</title>
@endsection
@section('css')

@endsection

@section('content')
    <div class="content-wrapper">

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4" style="padding: 30px;">
                        <div class="profile-img ">
                            <img  width="250" class="img-responsive" alt="" src="@if ($admin->image){{$admin->image}}@else {{ asset('upload/images/user.png') }} @endif">
                        </div>
                        <h3>{{ $admin->username }}</h3>
                        <form action="{{ route('admin.profile.updateimg',['admin' => $admin->id]) }}" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="form-group">
                                <input type="file" name="image" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="id" class="form-control" value="{{ $admin->id }}">
                            </div>
                            <button type="submit" class="btn btn-success">Update Image</button>
                        </form>
                        <ul class="list-unstyled user_data">
                            <li><i class="fa fa-envelope-o"></i> {{ $admin->email }}
                            </li>
                            <li><i class="fa fa-calendar"></i> {{ $admin->created_at->format("d-m-Y") . " ( " . $admin->created_at->diffForHumans() ." )" }}
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-8">
                        <h3 class="h3-title">Profile:</h3>
                        <hr>
                        @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        @foreach ($errors->all() as $er)
                                            {{ $er }} <br>
                                        @endforeach
                                    </div>
                                @endif
                                @if (session('thongbao'))
                                    <div class="alert alert-success">
                                        {{ session('thongbao') }}
                                    </div>
                                @endif
                        <form data-parsley-validate action="{{ route('admin.profile.update',['admin' => $admin->id]) }}" method="POST">
                            @csrf
                            <label for="fullname">Name:</label>
                            <input type="hidden" value="{{ $admin->id }}" name="id">
                            <input type="text" id="username" class="form-control" name="username" value="{{ $admin->username }}" required />
                
                            <label for="email">Email:</label>
                            <input type="email" id="email" class="form-control" name="email" value="{{ $admin->email }}" />
                            
                            <label for="password">Password:</label>
                            <input type="password" id="password" class="form-control" value="" name="password" />

                            <label for="reemail">Re - Password :</label>
                            <input type="password" id="repassword" class="form-control" value="" name="password_confirmation" />
                            
                            <br/>
                            <button type="submit" class="btn btn-success btn-user-update">Update</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
