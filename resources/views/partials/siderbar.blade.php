  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="" class="brand-link">
          <img src="adminlte/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
              style="opacity: .8">
          <span class="brand-text font-weight-light">AdminLTE</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
          <!-- Sidebar user panel (optional) -->
          @if (Auth::guard('admin')->check())
              <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                  <div class="image">
                      <img src="{{ (!empty($guard->image)) ? asset($guard->image) : asset('upload/images/user.png')}}" class="img-circle elevation-2" alt="User Image">
                  </div>
                  <div class="info">
                      <a href="{{ route('admin.profile', ['admin' => $guard->id]) }}"
                          class="d-block">{{ $guard->username }}</a>
                  </div>
              </div>
          @endif

          <!-- Sidebar Menu -->
          <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                  data-accordion="false">
                  <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                  @if ($guard->hasPermission('read-news'))
                      <li class="nav-item">
                          <a href="{{ route('news.manager') }}" class="nav-link">
                              <i class="nav-icon fas fa-th"></i>
                              <p>
                                  Danh mục bài báo
                                  <span class="right badge badge-danger">New</span>
                              </p>
                          </a>
                      </li>
                  @endif

                  @if ($guard->hasPermission('read-statistical'))
                      <li class="nav-item">
                          <a href="{{ route('admin.statistical.ctv') }}" class="nav-link">
                              <i class="nav-icon fas fa-th"></i>
                              <p>
                                  Báo cáo, thống kê
                              </p>
                          </a>
                      </li>
                  @endif

                  @if ($guard->hasRole('administrator'))
                      <li class="nav-item">
                          <a href="{{ route('admin.admins.index') }}" class="nav-link">
                              <i class="nav-icon fas fa-th"></i>
                              <p>
                                  Thành viên
                              </p>
                          </a>
                      </li>
                      <li class="nav-item">
                          <a href="{{ route('admin.admins.roles') }}" class="nav-link">
                              <i class="nav-icon fas fa-th"></i>
                              <p>
                                  Phân quyền
                              </p>
                          </a>
                      </li>
                  @endif
              </ul>
          </nav>
          <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
  </aside>
