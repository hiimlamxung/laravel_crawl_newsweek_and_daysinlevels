<div>
    <div class=" row col-md-12 d-flex justify-content-center">
        <div class="dataTables_info">
            Đang hiển thị <strong>{!! ($data->total() > $data->perPage()) ? $data->perPage() : $data->total()  !!}</strong> trên tổng số <strong>{!! $data->total() !!}</strong> bản ghi
        </div>
    </div><br>
    <div class="row col-md-12 d-flex justify-content-center">
        <div class="dataTables_paginate paging_simple_numbers">
            {!! $data->links() !!}
        </div>
    </div>
</div>