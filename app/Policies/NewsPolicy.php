<?php

namespace App\Policies;

use App\Models\Admins\Admin;
use App\Models\News;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NewsPolicy
{
    use HandlesAuthorization;


    public function editNews(Admin $admin,News $news)
    {
        // return auth()->guard('admin')->user()->id == $news->user_id;
        return true;
        return $admin->id == $news->user_id;
    }

}
