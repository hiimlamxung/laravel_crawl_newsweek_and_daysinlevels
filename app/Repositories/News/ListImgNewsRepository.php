<?php
namespace App\Repositories\News;

use App\Core\Repositories\BaseRepository;
use App\Models\News;
use App\Models\ListImgNews;
use DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use File;

class ListImgNewsRepository extends BaseRepository {
     protected $model;

     public function __construct(ListImgNews $ListImgNews)
     {
         parent::__construct($ListImgNews);
         $this->model = $ListImgNews;

     }

     public function upload_listimg($list_file,$news_id)
     {
         foreach($list_file as $key => $file){
             $filename = 'Img_content_' . time() . Str::random(10) . '.' . $file->getClientOriginalExtension();
             $path = $this->saveImage($file, $filename,'upload/images/news/');
             $params = [
                 'news_id'   => $news_id,
                 'path'      => $path
             ];
             $add = $this->model->create($params);
             if(!$add){
                 return false;
             } 
         }
         return true;
     }



     //del list img
     public function delete_list($news_id)
     {
        $list =  $this-> get_list_img($news_id);

        $del = $this->model->where('news_id',$news_id)->delete();
        //Xóa ảnh
        foreach($list as $item){
            if(file_exists(public_path($item->path))){
                File::delete(public_path($item->path));
            }
        }
        if($del){
            return true;
        }
        return false;
     }

     public function get_list_img($news_id)
     {
         return $list =  $this->model->where('news_id',$news_id)->get();
     }

}