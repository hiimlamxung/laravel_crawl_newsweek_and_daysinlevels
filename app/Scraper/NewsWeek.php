<?php

namespace App\Scraper;

use Goutte\Client;
use PHPOnCouch\CouchClient;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Support\Facades\File;

class NewsWeek extends CrawlerFunction
{
    protected const url = 'https://www.newsweek.com/world';
    protected const url_news_prefix = 'https://www.newsweek.com/';

    protected $client;
    protected $couch;
    protected $output;
    protected $message;
    protected $newsID;
    private $newsEditor;

    public function __construct()
    {
        $this->client = new Client();
        $this->output = new ConsoleOutput();
        $this->newsEditor = new NewsEditor();
        $this->couch = new CouchClient('http://' . config('couchdb.username') . ':' . config('couchdb.password') . '@' . config('couchdb.host') . ':' . config('couchdb.port'), config('couchdb.dbname'));
    }


    public function scraper()
    {
        // Get news editor
        $this->newsEditor->get_news_today();

        //get news of newsweek
        $crawler = $this->get_content_html(self::url);
        if ($crawler !== false) {
            $list = $this->get_news($crawler);
            $total = $list;
            if($total){
                push_fcm($this->message ,$total, $this->newsID, 'normal');
                $this->output->writeln("Pushed notification $total newsweek");
            }
        } else {
            $this->output->writeln("Fail !!!");
        }
    }

    private function exists_news($link)
    {
        $doc = $this->couch->key($link)->getView('search', 'link');
        return ($doc->rows) ? true : false;
    }

    private function get_news($crawler)
    {
        $total = 0;
        try {
            $crawler->filter('article')->each(function (Crawler $node) use (&$total) {
                $link = self::url_news_prefix . $node->filter('.inner>h3>a')->attr('href');

                if (!$this->exists_news($link)) {
                    $crawler_detail_new     = $this->get_content_html($link);
                    $handle_title           = $this->insert_span($node->filter('.inner>h3>a')->text());
                    $handle_description     = $this->insert_span($node->filter('.inner>.summary')->text());

                    $detail_news            = $this->get_detail_new($crawler_detail_new);
                
                    //merg all level word of title + description + content to 1 array;
                    $levelword_title        = $handle_title['level_word'];
                    $levelword_description  = $handle_description['level_word'];
                    $levelword_content      = $detail_news['level_word'];
                
                    $merge_level_word        = $this->Array_merge_word($levelword_title, $levelword_description, $levelword_content);
                    //add unknown word
                    $add_unknown = $this->add_UnknownField($merge_level_word);
                    //add count word->done.
                    $add_count = $this->add_CountWord($add_unknown);
                    $detail_word = $add_count;

                    if (str_word_count($detail_news['contents'], 0) < 2500) {

                        $new = [
                            'title'         => $handle_title['text'],
                            'link'          => $link,
                            'description'   => $handle_description['text'],
                            'img'           => ($node->filter('.image>picture img')->attr('src') != null) ? $node->filter('.image>picture img')->attr('src') : $node->filter('.image>picture img')->attr('data-src'),
                            'author'        => trim($detail_news['author']),
                            'pubDate'       => $detail_news['pubDate'],
                            'detail'        => [
                                'content'   => $detail_news['contents'],
                                'images'    => $detail_news['images'],
                                'video'     => null
                            ],
                            'type'          => 'normal',
                            'level_toeic'   => $detail_word['level_toeic'],
                            'level_ielts'   => $detail_word['level_ielts'],
                            'level_toefl'   => $detail_word['level_toefl'],
                            'toeic'         => $detail_word['toeic'],
                            'ielts'         => $detail_word['ielts'],
                            'toefl'         => $detail_word['toefl'],
                        ];
                        $add = $this->store_news($new);
                        if ($add) {
                            $total += 1;
                        }
                    }   
                }
            });
            $this->output->writeln("Import $total news from Newsweek!!!");
        } catch (\Exception $e) {
            $this->output->writeln("Loi1 " . $e->getMessage() . " Line: " . $e->getLine() . "file: " . $e->getFile());
        }
        return $total;
    }

    public function get_detail_new($crawler)
    {
        try {
            $related_string = ($crawler->filter('.related2')->count()>0) ? $crawler->filter('.related2')->text() : '';
            $title          = ($crawler->filter('.article-header>.title')->text()) ? $crawler->filter('.article-header>.title')->text() : "";
            //get content
            $contents       = $crawler->filter('.article-body')->text();
            $handle_contents = $this->handle_content($related_string, $contents);
            $contents       = $handle_contents['text'];
            $level_word     = $handle_contents['level_word'];
            // dd($crawler->filter('.byline .author>span')->text());
            $author = ($crawler->filter('.byline>span.author>span')->count()>1) ? trim($crawler->filter('.byline>span.author>span')->text()) : "Admin";
        
            $images = array();
            if (count($crawler->filter('.imgPhoto')) > 0) {
                $crawler->filter('.imgPhoto')->each(function (Crawler $node) use (&$images) {
                    array_push($images, $node->attr('src'));
                });
            } else $iamges = [];
            $pubDate   = ($crawler->filter('.byline>time')) ? date('Y-m-d H:i:s', $crawler->filter('.byline>time')->attr('data-timestamp')) : "";


            $detail_news = [
                'contents'          => $contents,
                'author'            => $author,
                'pubDate'           => $pubDate,
                'images'            => $images,
                'level_word'        => $level_word
            ];
            return  $detail_news;
        } catch (\Exception $e) {
            $this->output->writeln("Loi2 " . $e->getMessage() . " Line: " . $e->getLine() . "file: " . $e->getFile());
            return false;
        }
        return null;
    }

    private function store_news(array $new)
    {
        try {
            $add_new = $this->couch->storeDoc((object)$new);
            if (isset($add_new->ok) && $add_new->ok == true) {
                $audioFileName  = $this->getAudioFileName($new['detail']['content']);
                $audio          = $this->getAudio($audioFileName, $add_new->id);

                $doc = $this->couch->getDoc($add_new->id);
                $doc->detail->audio = $audio;
                $update = $this->couch->storeDoc($doc);

                $this->message = strip_tags($new['title']);
                $this->newsID = $add_new->id;

                return true;
            } else return false;
        } catch (\Exception $e) {
            $this->output->writeln($e->getMessage() . 'Line: ' . $e->getLine());
            return false;
        }
    }

    public function handle_content($related_string = null, $content)
    {
        $content = strip_tags($content);
        if ($related_string != null) {
            $content = str_replace($related_string, '', $content);
        }
        $content = preg_replace("/(\n|\t)/", '', $content);
        $content = $this->insert_span($content);
        return $content;
    }

}
