<?php
namespace App\Scraper;

use App\Models\News;
use App\Models\ListImgNews;
use Carbon\Carbon;
use Goutte\Client;
use PHPOnCouch\CouchClient;
use Symfony\Component\Console\Output\ConsoleOutput;

class NewsEditor extends CrawlerFunction{
    private $client;
    private $couch;
    private $output;

    public function __construct()
    {
        $this->client = new Client();
        $this->output = new ConsoleOutput();
        $this->couch = new CouchClient('http://' . config('couchdb.username') . ':' . config('couchdb.password') . '@' . config('couchdb.host') . ':' . config('couchdb.port'), config('couchdb.dbname'));
    }

    public function get_news_today()
    {
        $total = 0;

        $today = Carbon::now()->toDateString();
        $news = News::where('status',News::PUBLISH)->where('pub_date','like',"%$today%")->get();
        foreach($news as $item){
            if(!$this->exists_news($item->link)){
                
                $handle_content     = $this->handle_content($item->content);
                $handle_title       = $this->handle_content($item->title);
                $handle_description = $this->handle_content($item->description);
                
                $levelword_title        = $handle_title['level_word'];
                $levelword_description  = $handle_description['level_word'];
                $levelword_content      = $handle_content['level_word'];
                $merge_level_word       = $this->Array_merge_word($levelword_title, $levelword_description, $levelword_content);
                //add unknown word
                $add_unknown = $this->add_UnknownField($merge_level_word);
                //add count word->done.
                $add_count = $this->add_CountWord($add_unknown);
                $detail_word = $add_count;

                $new_item = $item; 
                $new_item->title        = $handle_title['text'];
                $new_item->description  = $handle_description['text'];
                $new_item->content      = $handle_content['text'];

                //get list img
                $list_img = ListImgNews::where('news_id',$item->id)->get();
                $list_img_arr = [];
                foreach($list_img as $img){
                    $list_img_arr[] = $img->path;
                }
                $doc = $this->create_doc($new_item,$list_img_arr,$detail_word);
                $store_doc = $this->couch->storeDoc((object)$doc);

                if(isset($store_doc->ok)){
                    $newsID = $store_doc->id;
                    $total += 1;
                    //update status field on mysql
                    News::where('id',$item->id)->update(['status' => 2]);

                    // get audio
                    $audio_file_name = $this->getAudioFileName($new_item->content);
                    $audio          = $this->getAudio($audio_file_name, $newsID);
                    //update audio field on couchDB
                    $doc = $this->couch->getDoc($store_doc->id);
                    $doc->detail->audio = $audio;
                    $update = $this->couch->storeDoc($doc);

                }
            }
        }
        $this->output->writeln("Import $total news editor");
    }

    private function create_doc($news,$list_img_arr,$detail_word){
        $doc = [
            'title'         => $news->title,
            'link'          => $news->link,
            'description'   => $news->description,
            'img'           => $news->image,
            "author"        => $news->admins->username,
            'pubDate'       => $news->pub_date,
            "detail"        =>  [
                "content"   =>  $news->content,
                "images"    =>  $list_img_arr,
                "video"     =>  $news->video,
            ],
            "type"  =>  "normal",
        ];
        $doc = array_merge($doc,$detail_word);
        return $doc;
    }

    private function exists_news($link)
    {
        $doc = $this->couch->key($link)->getView('search', 'link');
        return ($doc->rows) ? true : false;
    }

    private function handle_content($content)
    {
        $content = html_entity_decode($content); //Chuyển các ký tự từ dạng thực thể sang dạng html
        $content = strip_tags($content);
        $content = preg_replace("/(\n|\t)/", '', $content);
        $content = $this->insert_span($content);

        return $content;
    }   
}
