<?php

namespace App\Scraper;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class CrawlerFunction
{

    public function get_content_html($url)
    {
        $crawler = $this->client->request('GET', $url);
        $response = $this->client->getResponse();
        if ($response->getStatusCode() == 200) {
            return $crawler;
        }
        return false;
    }

    public function getAudioFileName($content)
    {
        $content = urlencode(strip_tags($content));

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://readspeaker.jp/tomcat/servlet/vt',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'text=' . $content . '&talkID=103&volume=100&speed=100&pitch=100&feeling=2&dict=0',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        if ($response) {
            $audio_file_name = preg_replace('/(comp=|\r|\n)/', '', $response);
            return $audio_file_name;
        } else return null;
    }

    public function getAudio($audio_file_name, $id)
    {
        try {
            $url_get_audio = 'https://readspeaker.jp/ASLCLCLVVS/JMEJSYGDCHMSMHSRKPJL/' . $audio_file_name;
            $curl = curl_init();

            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_URL, $url_get_audio);
            curl_setopt($curl, CURLOPT_TIMEOUT, 0);
            $data = curl_exec($curl);
            curl_close($curl);

            if (!file_exists('public/upload/audio/news')) {
               mkdir('public/upload/audio/news', 0777, true); //true: cho phép tạo folder lồng nhau
            }
            $name = $id . '.mp3';
            $path = 'public/upload/audio/news/' . $name;
            $put = File::put($path, $data);

            //Check size file
            // if (File::size($path) > 5000000 && File::exists($path)) {
            //     File::delete($path);
            // }
            if (File::exists($path)) {
                return $path;
            } else return null;
        } catch (\Exception $e) {
            $this->output->writeln($e->getMessage() . 'Line: ' . $e->getLine() . ' File:' . $e->getFile());
            return false;
        }
    }

    public function handle_trim($str)
    {
        $pattern = '/[^a-zA-Z0-9]+/';
        $check = preg_match_all($pattern, $str, $match);
        if ($check) {
            foreach ($match[0] as $trim_str) {
                $str = trim($str, addcslashes($trim_str, '.')); // trim sẽ hiểu dấu .. là ký tự đặc biệt -> lỗi. nên dùng addcslashes ('\.\.')
            }
        }
        return $str;
    }

    public function Array_merge_word(array $arr_title, array $array_description, array $array_content)
    {
        try {
            foreach ($arr_title as $key => $value) {
                foreach ($value as $k => $v) {
                    $arr_title[$key][$k] = array_merge($arr_title[$key][$k], $array_description[$key][$k], $array_content[$key][$k]);
                }
            }
            foreach ($arr_title as $key => $value) {
                foreach ($value as $k => $v) {
                    $arr_title[$key][$k] = array_unique($arr_title[$key][$k]);
                    $arr_title[$key][$k] = array_values($arr_title[$key][$k]);
                    // sort($arr_title[$key][$k]);
                }
            }
            return $arr_title;
        } catch (\Exception $e) {
            $this->output->writeln($e->getMessage() . 'Line: ' . $e->getLine() . ' File:' . $e->getFile());
            return false;
        }
    }
    
    public function insert_span($string)
    {
        $string = preg_replace("/(\n|\t)/", '', $string);
        $text = '';
        $result = [];
        $level_word = [
            'level_toeic' => [
                '1'         =>  [],
                '2'         =>  [],
                '3'         =>  [],
                '4'         =>  [],
                'unknown'   =>  [],
            ],
            'level_ielts' => [
                '1'         =>  [],
                '2'         =>  [],
                '3'         =>  [],
                '4'         =>  [],
                'unknown'   =>  [],
            ],
            'level_toefl' => [
                '1'         =>  [],
                '2'         =>  [],
                '3'         =>  [],
                '4'         =>  [],
                'unknown'   =>  [],
            ]
        ];

        $arr_str = explode(' ', $string);
        foreach ($arr_str as $key => $value) {
            $trim_value = $this->handle_trim($value);

            if (is_english($trim_value)) {
                $add_span   = $this->add_html_span($trim_value);
                $word_span = str_replace($trim_value, $add_span['text'], $value);
                $level_word = $this->add_levelword($level_word, $add_span);
                $word_add = ' ' . $word_span;
            } else {
                $word_add = ' ' . $value;
            }
            $text .= $word_add;
        }

        //del value unique
        foreach ($level_word as $key_type => $val_type) {
            foreach ($val_type as $key_level => $val_level) {
                $level_word[$key_type][$key_level] = array_unique($val_level);
                //hàm array_unique lúc này làm cho các key về kiểu string, dùng array_values để về kiểu int.
                $level_word[$key_type][$key_level] = array_values($level_word[$key_type][$key_level]);
            }
        }

        return $result = [
            'text'          => $text,
            'level_word'    => $level_word
        ];
    }

    public function add_html_span($str)
    {
        $lower_str = strtolower($str);
        $class = 'class="';
        $arr_class = $result = [];

        $toeic = json_decode(File::get(app_path('JsonWord/Toeic.json')), true);
        $ielts = json_decode(File::get(app_path('JsonWord/Ielts.json')), true);
        $toefl = json_decode(File::get(app_path('JsonWord/Toefl.json')), true);

        if (isset($toeic[$lower_str]) || isset($ielts[$lower_str]) || isset($toefl[$lower_str])) {
            if (isset($toeic[$lower_str])) {
                array_push($arr_class, 'toeic-' . $toeic[$lower_str]);
                $result['type']['level_toeic']   = $toeic[$lower_str];
            }
            if (isset($ielts[$lower_str])) {
                array_push($arr_class, 'ielts-' . $ielts[$lower_str]);
                $result['type']['level_ielts']   = $ielts[$lower_str];
            }
            if (isset($toefl[$lower_str])) {
                array_push($arr_class, 'toefl-' . $toefl[$lower_str]);
                $result['type']['level_toefl']   = $toefl[$lower_str];
            }
            $str_class = implode(' ', $arr_class);
            $class .= $str_class . '"';
        } else {
            $class .= 'unknown"';
        }

        $str = '<span ' . $class . '>' . $str . '</span>';
        $result['text'] = $str;
        return $result;
    }

    public function add_levelword(array $level_word = [], array $add_span)
    {
        if (isset($add_span['type'])) {
            foreach ($add_span['type'] as $key => $value) {
                array_push($level_word[$key][$value], strtolower(strip_tags($add_span['text'])));
            }
        }
        return $level_word;
    }

    public function add_UnknownField(array $level_word)
    {
        $toeic_word = array_map('strtolower', array_collapse($level_word['level_toeic']));
        $ielts_word = array_map('strtolower', array_collapse($level_word['level_ielts']));
        $toefl_word = array_map('strtolower', array_collapse($level_word['level_toefl']));
        $collect = array_unique(array_merge($toeic_word, $ielts_word, $toefl_word));

        $diff_toeic = array_values(array_diff($collect, $toeic_word));
        $diff_ielts = array_values(array_diff($collect, $ielts_word));
        $diff_toefl = array_values(array_diff($collect, $toefl_word));

        $level_word['level_toeic']['unknown'] = $diff_toeic;
        $level_word['level_ielts']['unknown'] = $diff_ielts;
        $level_word['level_toefl']['unknown'] = $diff_toefl;
        return $level_word;
    }

    public function add_CountWord(array $level_word)
    {
        foreach ($level_word['level_toeic'] as $key => $value) {
            $level_word['toeic'][$key] = count($level_word['level_toeic'][$key]);
        }
        foreach ($level_word['level_ielts'] as $key => $value) {
            $level_word['ielts'][$key] = count($level_word['level_ielts'][$key]);
        }
        foreach ($level_word['level_toefl'] as $key => $value) {
            $level_word['toefl'][$key] = count($level_word['level_toefl'][$key]);
        }

        return $level_word;
    }

}
