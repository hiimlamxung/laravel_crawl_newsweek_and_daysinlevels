<?php

namespace App\Scraper;

use Illuminate\Http\Request;
use Goutte\Client;
use PHPOnCouch\CouchClient;
use SebastianBergmann\Environment\Console;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;

class Daysinlevels extends CrawlerFunction
{
    protected const url = 'https://www.daysinlevels.com/';

    protected $client;
    protected $couch;
    protected $output;

    public function __construct()
    {
        $this->output   = new ConsoleOutput();
        $this->client   = new Client();
        $this->couch    = new CouchClient('http://' . config('couchdb.username') . ':' . config('couchdb.password') . '@' . config('couchdb.host') . ':' . config('couchdb.port'), config('couchdb.dbname'));
    }

    public function scraper()
    {
        $crawler = $this->get_content_html(self::url);
        if ($crawler !== false) {
            $list = $this->get_news($crawler);
            $total = $list;
            if($total){
                push_fcm($this->message ,$total, $this->newsID, 'easy');
                $this->output->writeln("Pushed notification $total daysynlevel");
            }
        } else {
            $this->output->writeln("Fail !!!");
        }
    }


    private function exists_news($link)
    {
        $doc = $this->couch->key($link)->getView('search', 'link');
        return ($doc->rows) ? true : false;
    }

    private function get_news($crawler)
    {
        $total = 0;
        try {
            $crawler->filter('.news-block')->each(function (Crawler $node) use (&$total) {
                $link = $node->filter('.news-block-right>.title>h3>a')->attr('href');
                if (!$this->exists_news($link)) {
                    $pubDate = date("Y-m-d H:i:s", strtotime(substr(trim($node->filter('.news-block-right>.news-excerpt')->text()), 0, 10)));
                    if($pubDate < Carbon::now()->subDays(30)){
                        $pubDate = Carbon::now();
                    }
                    $crawler_detail_new = $this->get_content_html($link);

                    $handle_title           = $this->insert_span($node->filter('.news-block-right>.title>h3>a')->text());
                    $handle_description     = $this->insert_span($node->filter('.news-block-right>.news-excerpt>p')->text());
                    $detail_news            = $this->get_detail_new($crawler_detail_new);
                     //merg all level word of title + description + content to 1 array;
                     $levelword_title        = $handle_title['level_word'];
                     $levelword_description  = $handle_description['level_word'];
                     $levelword_content      = $detail_news['level_word'];
                     $merge_level_word        = $this->Array_merge_word($levelword_title, $levelword_description, $levelword_content);

                    //add unknown word
                    $add_unknown = $this->add_UnknownField($merge_level_word);
                    //add count word->done.
                    $add_count = $this->add_CountWord($add_unknown);
                    $detail_word = $add_count;
                    if (str_word_count($detail_news['contents'], 0) < 2500) {
                        $new = [
                            'title'         => $handle_title['text'],
                            'link'          => $link,
                            'description'   => $handle_description['text'],
                            'img'           => $node->filter('.img-wrap>a>img')->attr('src'),
                            'author'        => null,
                            'pubDate'       => $pubDate,
                            'detail'        => [
                                'content'   => $detail_news['contents'],
                                'images'    => $detail_news['images'],
                                'video'     => null,
                            ],
                            'type'          => 'easy',
                            'level_toeic'   => $detail_word['level_toeic'],
                            'level_ielts'   => $detail_word['level_ielts'],
                            'level_toefl'   => $detail_word['level_toefl'],
                            'toeic'         => $detail_word['toeic'],
                            'ielts'         => $detail_word['ielts'],
                            'toefl'         => $detail_word['toefl']
                        ];
                        $add = $this->store_news($new);
                        if ($add) {
                            $total += 1;
                        }
                    }
                }
            });
            echo "Import $total news from Daysinlevels!!!";
        } catch (\Exception $e) {
            $this->output->writeln("Loi1 (Daysin) " . $e->getMessage() . " Line: " . $e->getLine() . "file: " . $e->getFile());
        }
        return $total;
    }

    public function get_detail_new($crawler)
    {
        try {
            $detail_new = array();
            $contents = $crawler->filter('.upper-content>#nContent')->text();
            $text_want_del = $crawler->filter('.upper-content>#nContent p:last-child')->text();
            $handle_contents = $this->handle_content($text_want_del, $contents);
            $contents       = $handle_contents['text'];
            $level_word     = $handle_contents['level_word'];
            $detail_news = [
                'contents'          => $contents,
                'author'            => null,
                'images'            => [],
                'level_word'        => $level_word
            ];
            return $detail_news;
        } catch (\Exception $e) {
            $this->output->writeln("Loi2 (Daysin) " . $e->getMessage() . " Line: " . $e->getLine() . "file: " . $e->getFile());
            return false;
        }
        return null;
    }

    private function store_news(array $new)
    {
        try {
            $add_new = $this->couch->storeDoc((object)$new);
            if (isset($add_new->ok) && $add_new->ok == true) {
                $audioFileName  = $this->getAudioFileName($new['detail']['content']);
                $audio          = $this->getAudio($audioFileName, $add_new->id);

                $doc = $this->couch->getDoc($add_new->id);
                $doc->detail->audio = $audio;
                $update = $this->couch->storeDoc($doc);

                $this->message = strip_tags($new['title']);
                $this->newsID = $add_new->id;

                return true;
            } else return false;
        } catch (\Exception $e) {
            $this->output->writeln($e->getMessage() . 'Line: ' . $e->getLine());
            return false;
        }
    }

    public function handle_content($text_want_del,$content)
    {
        $content = strip_tags($content);
        if ($text_want_del != null) {
            $content = str_replace($text_want_del, '', $content);
        }
        $content = preg_replace("/(\n|\t)/", '', $content);
        $content = trim(substr($content, 11));
        $content = $this->insert_span($content);
        return $content;
        
    }

    public function Array_merge_word(array $arr_title, array $array_description, array $array_content)
    {
        try {
            foreach ($arr_title as $key => $value) {
                foreach ($value as $k => $v) {
                    $arr_title[$key][$k] = array_merge($arr_title[$key][$k], $array_description[$key][$k], $array_content[$key][$k]);
                }
            }
            foreach ($arr_title as $key => $value) {
                foreach ($value as $k => $v) {
                    $arr_title[$key][$k] = array_unique($arr_title[$key][$k]);
                    $arr_title[$key][$k] = array_values($arr_title[$key][$k]);
                    // sort($arr_title[$key][$k]);
                }
            }
            return $arr_title;
        } catch (\Exception $e) {
            $this->output->writeln($e->getMessage() . 'Line: ' . $e->getLine() . ' File:' . $e->getFile());
            return false;
        }
    }
}
