<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Admins\Contract\AdminRepositoryInterface;
use App\Repositories\Admins\AdminRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(
            AdminRepositoryInterface::class,
            AdminRepository::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
