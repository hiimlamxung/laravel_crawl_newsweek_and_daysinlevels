<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Scraper\NewsWeek;
use App\Scraper\Daysinlevels;

class Scraper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scraper:news';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'crawl news from www.newsweek.com/world and daysinlevels.com';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $bot = new NewsWeek();
        $bot->scraper();

        $bot = new Daysinlevels();
        $bot->scraper();
    }
}
