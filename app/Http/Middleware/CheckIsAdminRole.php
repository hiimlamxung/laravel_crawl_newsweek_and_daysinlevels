<?php

namespace App\Http\Middleware;

use Closure;

class CheckIsAdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->guard('admin')->user();
        if($user->role !== 1){
            abort(403);
        }
        return $next($request);
    }
}
