<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule; // set my rules
use Illuminate\Http\Request;
use App\Models\News;
class ValidateFormEditNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
       return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title'         => ['required','min:10','unique:news,title,'. Request::post('id')], //loại trừ title của news có id hiện tại
            'link'          => ['required','min:10','unique:news,link,'. Request::post('id')],
            'pub_date'      => 'required',
            'image'         =>  'image|mimes:jpeg,png,jpg,gif,svg',
            'list_img.*'    => 'mimes:jpeg,png,jpg,gif,svg',
            'video'         =>  'mimes:mp4,mov,ogg,qt|max:20000',
            'description'   => 'required|min:100|max:2000',
            'content'       =>  'required|min:100|max:2000',
        ];
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors()->first(), 422));
    }
}
