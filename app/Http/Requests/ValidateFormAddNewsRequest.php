<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ValidateFormAddNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title'         => 'required|min:10',
            'link'          => 'required|min:10',
            'pub_date'      => 'required',
            'image'         =>  'required|image|mimes:jpeg,png,jpg,gif,svg',
            'list_img.*'    => 'image|mimes:jpeg,png,jpg,gif,svg',
            'video'         =>  'mimes:mp4,mov,ogg,qt|max:20000',
            'description'   => 'required|min:100|max:2000',
            'content'       =>  'required|min:100|max:2000',
        ];
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors()->first(), 422));
    }
}
