<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class EditAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'username' => 'required|min:3',
            'email' => ['required','email','unique:admins,email,'.$request->id],
            'role'  => 'required|numeric',
            'iamge' => 'image|mimes:png,jpg,gif'
        ];
    }
}
