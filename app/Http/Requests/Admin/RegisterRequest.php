<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required',
            'email'    => 'required|email|unique:admins,email',
            'password' => 'required|min:6|confirmed',      
        ];
    }

    public function messages()
    {
        return [
            'username.required' => 'Bạn chưa nhập tên',
            'email.required'    => 'Bạn chưa nhập email',
            'email.email'       => 'Email chưa đúng định dạng',
            'email.unique'      => 'Email đã có tài khoản sử dụng',
            'password.required' => 'Bạn chưa nhập mật khẩu',      
            'password.min'      => 'Mật khẩu tối thiểu ít nhất 6 ký tự',      
            'password.confirmed'=> 'Nhập lại mật khẩu chưa đúng',      
        ];
    }
}
