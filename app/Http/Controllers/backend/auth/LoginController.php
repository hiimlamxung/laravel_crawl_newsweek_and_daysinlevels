<?php

namespace App\Http\Controllers\backend\auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LoginRequest;
use App\Core\Traits\Authorization;

class LoginController extends Controller
{
    //
    use Authorization;
    public function index()
    {
        return view('admin/login');
    }

    public function login(LoginRequest $request)
    {
        $remember = $request->remember;
        $params = $request->only(['email', 'password', 'active']);
        if ($this->guard()->attempt($params,$remember)) {
            return redirect()->route('home.admin');
        }
        return redirect()->back()->withErrors('Đăng nhập thất bại');
    }

    public function logout()
    {
        if($this->guard()->check()){
            $this->guard()->logout();
        }
        return redirect()->route('login.admin');
    }
}
