<?php

namespace App\Http\Controllers\backend\auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RegisterRequest;
use App\Models\Admins\Admin;

class RegisterController extends Controller
{
    //
    public function index()
    {
        return view('admin/register');
    }

    public function register(RegisterRequest $request)
    {
       $params = [
           'username'   => $request->username,
           'email'      => $request->email,
           'password'   =>bcrypt( $request->password),
           'active'     => 0,
       ];

        $register = Admin::create($params);
        if(!$register){
            return back()->withErrors('Có lỗi trong quá trình đăng ký, vui lòng thử lại.');
        }
        return back()->with('thongbao','Đăng ký thành công.');
    }
}
