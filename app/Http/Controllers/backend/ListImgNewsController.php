<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\News\ListImgNewsRepository;
use App\Models\ListImgNews;
use Illuminate\Support\Facades\File;

class ListImgNewsController extends Controller
{
    protected $ListImgNews;
    public function __construct(ListImgNewsRepository $ListImgNews)
    {
        $this->ListImgNews = $ListImgNews;
    }

    public function delete_img(ListImgNews $id)
    {
        if(file_exists(public_path($id->path))){
            File::delete(public_path($id->path));
        }
        $del = $id->delete();
        if($del){
            return response()->json('Xoá thành công',200);
        }else{
            return response()->json('Xoá thất bại',500);
        }
        
    }
}
