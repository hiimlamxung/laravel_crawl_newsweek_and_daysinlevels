<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admins\Admin;
use App\Models\Admins\Role;
use App\Models\Admins\Permission;
use App\Core\Traits\Authorization;
use App\Repositories\Admins\Contract\AdminRepositoryInterface;
use App\Repositories\Admins\AdminRepository;
use Response;
use App\Http\Requests\Admin\StoreAdminRequest;
use App\Http\Requests\Admin\EditAdminRequest;
use File;
use Illuminate\Support\Facades\Gate;
use Auth;
use Javascript;

class AdminController extends Controller
{
    //
    use Authorization;
    public function __construct(AdminRepositoryInterface $admin)
    {
        $this->admin = $admin;
    }
    public function index()
    {  
        $lists = $this->admin->getAllWithPaginate($filter = [], $with=['roles','permissions'], $pageSize = 10, $orderby = ['created_at' => 'DESC']);
        $roles = Role::all();
        $permissions = Permission::all();
        Javascript::put([
            'activeorstop' => route('admin.admins.activeorstop'),
            'data'  => $lists,
            'link_update_admin_role' => route('admin.admins.roles.update'),
            'link_update_admin_permission' => route('admin.admins.permission.update')
        ]);
        return view('admin.admins.index',compact('lists','roles','permissions'));
    }

    public function updateRoleAdmin(){
        $admin = Admin::findOrFail(request('id'));
        $roles = request('roles');
        if(!empty($roles)){
            // sync role and permissions
            $admin->syncRoles($roles);
            $admin->syncPermissions($admin->getPermissionsViaRoles());
        }else{
            $permissionsInRoles = $admin->getPermissionsViaRoles();
            $admin->detachRoles();
            $admin->detachPermissions($permissionsInRoles);
        }

        return response()->json('success',200);
    }

    public function updatePermissionAdmin(){
        $admin = Admin::findOrFail(request('id'));
        $permissions = request('permissions');
        if(!empty($permissions)){
            // sync role and permissions
            $admin->syncPermissions($permissions);
        }else{
            $admin->detachPermissions();
        }

        return response()->json('success',200);
    }

    public function create()
    {
        $roles = Role::all()->sortByDesc('id');
        return view('admin.admins.create',compact('roles'));
    }

    public function store(StoreAdminRequest $request)
    {
        $params = [
            'username' => $request->username,
            'email' => $request->email,
            'password' => $request->password,
            'role' => $request->role,
            'action'    => 1,
        ];
        $create = $this->admin->create($params);
        if($create){
            $role = Role::with('permissions')->find($request->role);
            $create->attachRole($request->role);
            $create->attachPermissions($role->permissions);
            return redirect()->route('admin.admins.index')->with('thongbao','Thêm mới thành viên thành công');
        }else{
            return back()->withErrors('Có lỗi trong quá trình thêm');
        }
    }
    
    public function edit(Admin $id)
    {
        $admin = $id;
        return view('admin.admins.edit',compact('admin'));
    }
    
    public function update(EditAdminRequest $request,Admin $id)
    {
        $params = [
            'username' => $request->username,
            'email' => $request->email,
            'role' => $request->role,
        ];
        $update = $this->admin->updateInfor($id->id, $params);
        if($update){
            if($request->file('image')){
                $old_img = $id->image;

                $repo_admin = new AdminRepository($id);
                $update_img = $repo_admin->updateImage($request->image);

                //xoá ảnh cũ
                if(file_exists($old_img)){
                    File::delete($old_img);
                }
            }
            return back()->with('thongbao','Cập nhật thành công');
        }else{
            return back()->withErrors('Có lỗi trong quá trình cập nhật');
        }
    }

    public function delete(Admin $id)
    {
        $id = $id->load(['roles','permissions']);
        $repo_admin = new AdminRepository($id);
        $del_admin =  $repo_admin->delete_admin($id);
        return back()->with('thongbao','Xoá thành công !');
    }

    public function activeorstop(Request $request, Admin $id)
    {
        if($request->ajax()){
            if(is_null($id)){
                return response()->json('fail',500);
            }
            $active = $request->post('active');
            $id->active = (int)$active;
            $change = $id->save();

            if($change){
                return response()->json('success',200);
            }else{
                return response()->json('fail',500);
            }
        }
    }

    public function profile(Admin $admin)
    {
        return view('admin.admins.profile',compact('admin'));
    }

    public function updateProfile(UpdateProfileRequest $request, Admin $admin){
        $attributes = $request->only('username', 'password','email');
        foreach($attributes as $key => $value){
            if(empty($value)){
                unset($attributes[$key]);
                continue;
            }
            if($key == 'password'){
                $attributes[$key] = bcrypt($value);
            }
        }
        // dd($attributes);
        if(!empty($attributes)){
            $guard = $this->guard()->user();
            if($guard->can('update', $admin)){
                $repo = new AdminRepository($admin);
                $repo->update($attributes);
            }else abort(403);
        }
        return redirect()->back()->with('thongbao','Cập nhật thành công.');
    }

    public function updateImage(Request $request, Admin $admin){
        if($request->hasFile('image')){
            $old_img = $admin->image;
            $repo = new AdminRepository($admin);
            $repo->updateImage($request->file('image'));

            if(file_exists(public_path($old_img))){
                File::delete(public_path($old_img));
            }
        }
        return redirect()->back();
    }

    public function roles()
    {
        $roles = Role::with('permissions')->latest()->paginate();
        $permissions = Permission::all();

        JavaScript::put([
            'roles' => $roles,
            'link_update_permission_role' => route('admin.admins.roles.addpermissiontorole'),
            'link_delete_role' => route('admin.admins.roles.delete')
        ]);
        return view('admin.admins.roles',compact('roles','permissions'));
    }

    public function store_role(Request $request)
    {
        $params = $request->only(['name','display_name','description']);
        $add = Role::create($params);
        if($add){
            return back()->with('thongbao','Thêm quyền thành công');
        }
        return back()->with('thongbao','Thêm thất bại');
    }

    public function store_permission(Request $request)
    {
        $params = $request->only(['name','display_name','description']);
        $add = Permission::create($params);
        if($add){
            return back()->with('thongbao','Thêm chức năng thành công');
        }
        return back()->with('thongbao','Thêm thất bại');
    }

    public function addPermissionToRole(Request $request)
    {
        $role = Role::find($request->id);
        $permissions = $request->permissions;
        if($role){
            (!empty($permissions)) ? $role->syncPermissions($permissions) : $role->detachPermissions();
            return response()->json('successs',200);
        }
        return response()->json('error',400);
    }

    public function delete_role(Request $request)
    {
        $role = Role::find($request->id);
        if($role->delete()){
            return response()->json('successs',200);
        }
        return response()->json('error',400);
    }
}
