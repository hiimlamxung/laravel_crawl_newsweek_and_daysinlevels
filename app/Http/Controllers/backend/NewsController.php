<?php

namespace App\Http\Controllers\backend;

use App\Core\Repositories\InnerNews;
use App\Repositories\News\ListImgNewsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\ValidateFormAddNewsRequest;
use App\Http\Requests\ValidateFormEditNewsRequest;
use App\Models\News;
use App\Models\ListImgNews;
use Illuminate\Support\Facades\File;
use Javascript;

class NewsController extends Controller
{
    public function __construct(InnerNews $news)
    {
        $this->news = $news;
    }
    public function managerNews(Request $request)
    {
        $title = ($request->search) ? $request->search : '';
        $pub_date = ($request->pub_date) ? $request->pub_date : '';

        $param['pub_date'] = ['operator' => 'like', 'value' => "%$pub_date%"];

        $param['title'] = ['operator' => 'like', 'value' => "%$title%"];
        
        $with = ['admins'];
        $module = ($request->status) ? $request->status : 'all';
        switch($module){
            case 'new':
                $param['status'] = 0;
                break;
            case 'posted':
                $param['status'] = 1;
                break;
            case 'success':
                $param['status'] = 2;
                break;
            case 'deleted':
                $param['status'] = -1;
                break;
            case 'all':
                // $param['status'] = ['operator' => '<>','value' => -1];
                break;
            default:
                $module = $request->status = 'all'; 
        }

        $news = $this->news->getAllWithPaginate($param, $with, 10, ['created_at' => 'DESC']);
        //khi nhấn lọc
        $news = ($request->has('search') && $request->has('status') && $request->has('pub_date')) ? $news->appends(['search' => $title,'status' => $request->status,'pub_date' => $pub_date]) : $news;
        
        Javascript::put([
            'url_updatestt' => route('admin.news.updatestt'),
            'url_posted_listnews' => route('admin.news.postedlist'),
            'url_delete_listnews' => route('admin.news.dellist'),
        ]);
        return view('admin.news.manager',compact(['module','news']));
    }

    public function createNews()
    {
            Javascript::put([
                'store' => route('news.store'),
            ]);
            return view('admin.news.create');
    }

    public function storeNews(ValidateFormAddNewsRequest $request)
    {
            if($request->ajax()){
                $all            = Input::all();
                $title          = $all['title'];
                $link           = $all['link'];
                $pub_date       = str_replace('T',' ', $all['pub_date']) .':00';
                $description    = $all['description'];
                $content        = $all['content'];
                // dd($content);
                $image          = $all['image'];
                $list_img       = (isset($all['list_img'])) ? $all['list_img'] : null;
                $video          = (isset($all['video'])) ? $all['video'] : null;

                $user = auth()->guard('admin')->user();
                $data = [
                    'user_id'   =>  $user->id,
                    'title'     => $title,
                    'link'      => $link,
                    'pub_date'  => $pub_date,
                    'description' => $description,
                    'content'   => $content,
                ];
                $new = $this->news->createNews($user->id, $data);

                if($new == false){
                    return response()->json('News exists', 302);
                }else{
                    $inner      = new InnerNews($new);
                    $list_img_repo   = new ListImgNewsRepository(new ListImgNews);
                    if($image){
                        $update = $inner->upload_img($all['image']);
                    }
                    if($list_img ){
                        $add_img = $list_img_repo->upload_listimg($all['list_img'],$new->id);
                    }
                    if($video){
                        $update_video  = $inner->upload_video($all['video']);
                    }
                }
                return response()->json('success', 200);
            }else{
                return response()->json('Bad Request',401);
            }
    } 
    
    public function editNews(News $id)
    {
        $user = auth()->guard('admin')->user();
        if(($user->can('update-news') && ($user->id == $id->user_id)) || ($user->can('update-news') && $user->hasRole('administrator'))){
            $new = $id;
            Javascript::put([
                'update' => route('news.update'),
                'delimg' => route('admin.news.delimg'),
                'delvideo'  => route('admin.news.delvideo')
            ]);
            return view('admin.news.edit',compact(['new']));
        }else abort(403);
    }

    public function updateNews(ValidateFormEditNewsRequest $request,News $id)
    {
        $user = auth()->guard('admin')->user();
        if(($user->can('update-news') && ($user->id == $id->user_id)) || ($user->can('update-news') && $user->hasRole('administrator'))){
            if($request->ajax()){
                $all            = Input::all();
                $title          = $all['title'];
                $link           = $all['link'];
                $pub_date       = str_replace('T',' ', $all['pub_date']).':00';
                $description    = $all['description'];
                $content        = $all['content'];
                $status         = $all['status'];
                $image          = (isset($all['image'])) ? $all['image'] : null;
                $list_img       = (isset($all['list_img'])) ? $all['list_img'] : null;
                $video          = (isset($all['video'])) ? $all['video'] : null;
    
                $data = [
                    'title'     => $title,
                    'link'      => $link,
                    'pub_date'  => $pub_date,
                    'description' => $description,
                    'content'   => $content,
                    'status'    => $status
                ];
                if($image){
                    $old_image = $id->image;
                }
                if($video){
                    $old_video = $id->video;
                }
                $update = $this->news->updateNews($id->id, $data);
    
                if($update == false){
                    return response()->json('Can not update this new', 500);
                }
                else{
                    $inner          = new InnerNews($id);
                    $list_img_repo   = new ListImgNewsRepository(new ListImgNews);
    
                    //update image
                    if($image){
                        $update = $inner->upload_img($all['image']);
                        if(file_exists(public_path($old_image))){
                            File::delete(public_path($old_image));
                        }
                    }
                    if($list_img){
                        $add_img = $list_img_repo->upload_listimg($all['list_img'],$id->id);
                    }
                    if($video){
                        $update_video  = $inner->upload_video($all['video']);
                        if(file_exists(public_path($old_video))){
                            File::delete(public_path($old_video));
                        }
                    }
                }
                return response()->json('success', 200);
            }else{
                return response()->json('Bad Request',401);
            }
        }else abort(403);
    }
    
    public function deleteNews(News $id)
    {   
        $user = auth()->guard('admin')->user();
        if(($user->can('delete-news') && ($user->id == $id->user_id)) || ($user->can('delete-news') && $user->hasRole('administrator'))){
            $del = $this->news->delete_news($id);
            return back()->with('thongbao','Xoá thành công !');
        }else return abort(403);
    }

    public function delete_video(News $id)
    {
        $user = auth()->guard('admin')->user();
        if(($user->can('update-news') && ($user->id == $id->user_id)) || ($user->can('update-news') && $user->hasRole('administrator'))){
            if(file_exists(public_path($id->video))){
                File::delete(public_path($id->video));
            }
            $update = $id->update([
                'video' => null,
            ]);
            if($update){
                return response()->json('Xoá thành công',200);
            }else{
                return response()->json('Xoá thất bại',500);
            }
        }
    }

    public function update_stt(Request $request)
    {
        if($request->ajax()){
                    $id = $request->id;
                    $stt = $request->stt;
                    $new = News::find($id);
                    if($new->status==2 || !$new){
                        return response()->json('error',400);
                    }
                    if($stt == 2){
                        $user_id = auth()->guard('admin')->user()->id;
                        $params['user_post'] = $user_id;
                    }
    
                    $params['status'] = $stt;
                    $update = $this->news->updateNews($id, $params);
                    if($update){
                        return response()->json('success',200);
                    }
                    return response()->json('error',500);
        }
        
    }

    public function PostedListNews(Request $request)
    {
        if($request->ajax()){
                $list_id_news = $request->list_news;
                $list_news = News::whereIn('id',$list_id_news)->get();
                //loại bỏ news đã phát hành rồi
                foreach($list_news as $new){
                    if($new->status == 2){
                        $key = array_search($new->id,$list_id_news);
                        unset($list_id_news[$key]);
                    }
                }
                $user_id = auth()->guard('admin')->user()->id;
                $params['user_post'] = $user_id;
                $params['status'] = 1;
                // dd($list_id_news);
                if(!empty($list_id_news)){
                    $update = $this->news->updateListNews($list_id_news,$params);
                    if(!$update){
                        return response()->json('error',400);
                    }
                }
                return response()->json('success',200);
        }
        
    }

    public function DeleteListNews(Request $request)
    {
        if($request->ajax()){
                $list_id_news = $request->list_news;

                $del = $this->news->delete_list_news( $list_id_news);
                if($del){
                    return response()->json('success',200);
                }
                return response()->json('error',500);
        }
        
    }
}
