<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admins\Admin;
use App\Models\News;

class StatisticalController extends Controller
{
    public function __construct(){
        $this->admin = new Admin;
    }

    public function ctv(Request $request)
    {
        $success = News::SUCCESS;
        $deleted = News::DELETED;
        $publish = News::PUBLISH;
        $list_ctv = Admin::whereHas('roles',function($q){
            $q->where('name','editor');
        })->where('active',1)->get();
        
        $month  = ($request->month) ? $request->month : date('m');
        $month  = (strlen($month) == 1) ? '0'.$month : $month;

        $year   = ($request->year) ? $request->year : date('Y');

        $id     = ($request->id) ? $request->id : 'all';

        if($id != 'all'){
            $statistical_ctv = News::whereHas('admins' ,function($q) use ($id){
                $q->where('id',$id);
            });
        }else{
            $list_id_ctv = $list_ctv->pluck('id')->toArray();
            $statistical_ctv = News::whereHas('admins', function($q) use ($list_id_ctv){
                $q->whereIn('id',$list_id_ctv);
            });
        }

        $statistical_ctv = $statistical_ctv->where('pub_date','like',$year.'-'.$month.'%')->orderBy('created_at','DESC');
        $statistical_unlimit = $statistical_ctv->get();
        $statistical_pagi = $statistical_ctv->paginate(10)->appends(['month' => $month,'year' => $year, 'id' => $id]);
        
        $count['news'] = $count['success'] = $count['publish'] =  $count['deleted'] = 0;
        $collect_to_arr = $statistical_unlimit->toArray();
        foreach($collect_to_arr as $news){
                $count['news'] += 1;
                switch ($news['status']) {
                    case $success:
                        $count['success'] += 1;
                        break;

                    case $publish:
                        $count['publish'] += 1;
                        break;

                    case $deleted:
                        $count['deleted'] += 1;
                        break;
                }
        }
        return view('admin.statistical.ctv',compact(['list_ctv','statistical_pagi','month','year','id','success','deleted','count']));
    }

    public function censorship(Request $request)
    {
        $success = News::SUCCESS;
        $deleted = News::DELETED;
        $publish = News::PUBLISH;
        
        $month  = ($request->month) ? $request->month : date('m');
        $month  = (strlen($month) == 1) ? '0'.$month : $month;

        $year   = ($request->year) ? $request->year : date('Y');

        $id     = ($request->id) ? $request->id : 'all';
        
        $list_censorship =  Admin::whereHas('roles',function($q){
            $q->where('name','censorship');
        })->where('active',1)->get();
        // \DB::enableQueryLog();
        if($id != 'all'){
            $statistical_censor = News::where('user_post',$id);
        }else{
            $list_id_censor = $list_censorship->pluck('id')->toArray();
            $statistical_censor = News::whereIn('user_post',$list_id_censor);
        }

        $statistical_censor = $statistical_censor->where('pub_date','like',$year.'-'.$month.'%')->orderBy('created_at','DESC');
        $statistical_unlimit = $statistical_censor->get();
        // dd(\DB::getQueryLog());
        $statistical_pagi = $statistical_censor->paginate(10)->appends(['month' => $month,'year' => $year, 'id' => $id]);
        
        $count['news'] = $count['success'] = $count['publish'] =  $count['deleted'] = 0;
        $collect_to_arr = $statistical_unlimit->toArray();
        foreach($collect_to_arr as $news){
                $count['news'] += 1;
                switch ($news['status']) {
                    case $success:
                        $count['success'] += 1;
                        break;

                    case $publish:
                        $count['publish'] += 1;
                        break;

                    case $deleted:
                        $count['deleted'] += 1;
                        break;
                }
        }
        return view('admin.statistical.censorship',compact(['list_censorship','statistical_pagi','month','year','id','success','deleted','count']));
    }

}
