<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admins\Admin;
use App\Repositories\Admins\AdminRepository;
use File;
use App\Http\Requests\UpdateProfileRequest;
use Illuminate\Support\Facades\Gate;

class ProfileController extends Controller
{
    //
    public function profile(Admin $admin)
    {   
        $user = auth()->guard('admin')->user();
        if (Gate::forUser($user)->allows('profile', $admin)) {
            return view('admin.profile.profile',compact('admin'));
        }else abort(403);
    }

    public function updateProfile(UpdateProfileRequest $request, Admin $admin){
        $user = auth()->guard('admin')->user();
        if (Gate::forUser($user)->allows('profile', $admin)) {
            $attributes = $request->only('username', 'password','email');
            foreach($attributes as $key => $value){
                if(empty($value)){
                    unset($attributes[$key]);
                    continue;
                }
                if($key == 'password'){
                    $attributes[$key] = bcrypt($value);
                }
            }
            // dd($attributes);
            if(!empty($attributes)){
                $repo = new AdminRepository($admin);
                $repo->update($attributes);
            }
            return redirect()->back()->with('thongbao','Cập nhật thành công.');
        }else abort(403);
    }

    public function updateImage(Request $request, Admin $admin){
        $user = auth()->guard('admin')->user();
        if (Gate::forUser($user)->allows('profile', $admin)) {
            if($request->hasFile('image')){
                $old_img = $admin->image;
                $repo = new AdminRepository($admin);
                $repo->updateImage($request->file('image'));
    
                if(file_exists(public_path($old_img))){
                    File::delete(public_path($old_img));
                }
            }
            return redirect()->back();
        }else abort(403);
    }
}
