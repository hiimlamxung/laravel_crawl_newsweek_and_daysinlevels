<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admins\Admin;
use App\Models\ListImgNews;

class News extends Model
{
    //
    const CREATED = 0;
    const PUBLISH = 1;
    const DELETED = -1;
    const SUCCESS = 2;
    
    protected $table = 'news';
    protected $fillable  = ['user_id','pub_date','title','link','video','image','description','content','status','user_post','created_at'];
    protected $hidden = ['created_at', 'updated_at'];

    public function admins(){
        return $this->belongsTo(Admin::class, 'user_id');
    }

    public function adminAction(){
        return $this->belongsTo(Admin::class, 'user_post');
    }

    // public function comments(){
    //     return $this->hasMany(Comment::class, 'news_id', 'id');
    // }

    public function success(){
        $this->status = self::SUCCESS;
    }

    public function publish(){
        $this->status = self::PUBLISH;
    }

    public function ListImgNews()
    {
        return $this->hasMany(ListImgNews::class,'news_id','id');
    }
}
