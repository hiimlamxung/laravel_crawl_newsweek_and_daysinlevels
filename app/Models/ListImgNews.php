<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ListImgNews extends Model
{
    //
    protected $table = 'detail_img_news';
    protected $fillable = ['news_id','path'];

    
}
