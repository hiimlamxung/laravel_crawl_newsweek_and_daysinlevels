<?php

namespace App\Models\Admins;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\News;
use Laratrust\Traits\LaratrustUserTrait;

class Admin extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;
    use SoftDeletes;

    protected $table = 'admins';

    protected $fillable = ['username','email','password','image','role','active'];
    
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function news()
    {
        return $this->hasMany(News::class, 'user_id', 'id');
    }

    public function actionNews()
    {
        return $this->hasMany(News::class, 'user_post', 'id');
    }

    public function roles(){
        return $this->belongsToMany(Role::class)->withPivotValue('user_type', self::class);
    }

    public function permissions(){
        return $this->belongsToMany(Permission::class)->withPivotValue('user_type', self::class);
    }

    public function getPermissionsViaRoles(){
        return $this->load('roles', 'roles.permissions')
            ->roles->flatMap(function ($role) {
                return $role->permissions;
            })->sort()->values();
    }

    public function getAllPermissions(){
        $permissions = $this->permissions;
        
        if ($this->roles) {
            $permissions = $permissions->merge($this->getPermissionsViaRoles());
        }

        return $permissions->sort()->values();
    }
}
