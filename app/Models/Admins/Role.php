<?php

namespace App\Models\Admins;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    protected $table = 'roles';
    protected $fillable = ['name','display_name','description'];
    public function permissions(){
        return $this->belongsToMany(Permission::class);
    }
}