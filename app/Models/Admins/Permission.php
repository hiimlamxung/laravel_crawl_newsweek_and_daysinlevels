<?php

namespace App\Models\Admins;

use Laratrust\Models\LaratrustPermission;

class Permission extends LaratrustPermission
{
    protected $table = 'permissions';
    protected $fillable = ['name','display_name','description'];

    public function roles(){
        return $this->belongsToMany(Role::class);
    }
}