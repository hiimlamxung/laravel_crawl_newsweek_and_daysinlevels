<?php 
namespace App\Core\Repositories;

use App\Models\News;
use DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use App\Repositories\News\ListImgNewsRepository;
use App\Models\ListImgNews;
use Illuminate\Support\Facades\File;

class InnerNews extends BaseRepository {

    protected $model;

    public function __construct(News $news){
        // $this->model  = new News();
        parent::__construct($news);
        $this->model  = $news;
    }

    // public function createNewsWeekend($user_id, $news){
    //     if($this->checkNewsUnique($news['link'])){
    //         return false;
    //     }else{
    //         return $this->create([
    //             'user_id' => $user_id,
    //             'pub_date' => $news['pub_date'],
    //             'title'   => $news['title'],
    //             'description' => $news['description'],
    //             'image'   => $news['image'],
    //             'content' => $news['content'],
    //             'link'    => $news['link'],
    //             'video'   => $news['video'],
    //             // 'name_link'    => $news['name_link'],
    //             // 'kind'    => $news['kind'],
    //             // 'news_order'    => $news['news_order'],
    //             'created_at' => date('Y-m-d H:i:s')
    //         ]);
    //     }
    // }

    public function createNews($user_id,$news)
    {
        if($this->checkNewsUnique($news['link'])){
            return false;
        }else{
            return $this->model->create([
                'user_id' => $user_id,
                'pub_date' => $news['pub_date'],
                'title'   => $news['title'],
                'description' => $news['description'],
                'content' => $news['content'],
                'link'    => $news['link'],
            ]);
        }
    }

    public function updateListNews($array_id,$params)
    {
        return $this->model->whereIn('id',$array_id)->update($params);
    }

    public function updateNews($id, $params)
    {
        return $this->model->where('id',$id)->update($params);
    }

    public function checkNewsUnique($link){
        return $this->model->whereLink($link)->exists();
    }

    public function getDescription($id){
        return $this->model->where('id', $id)->select('description')->first();
    }
    
    public function getContent($id){
        return $this->model->where('id', $id)->select('content')->first();
    }

    public function totalNewsForDate($date){
        return $this->model->where('pub_date', $date)->count();
    }

    public function getDetailNews($id){
        return $this->model->with(['comments' => function($q) {
                                $q->with('user');
                            }])->find($id);
    }

    public function updateParams($params, $condition) {
        return $this->model->where($condition)->update($params);
    }

    public function checkLinkDuplicate($link) {
        return $this->model->where('link', $link)->whereIn('status', [0,1,2])->count();
    }

    public function upload_img($file) //param $field is column to update
    {
        $filename = 'Thumb_image_' . time() . Str::random(10) . '.' . $file->getClientOriginalExtension();
        $path = $this->saveImage($file, $filename,'upload/images/news/');
        return $this->model->update([
            'image' => $path
        ]);
    }
    public function upload_video($file) //param $field is column to update
    {
        $filename = 'Video_' . time() . Str::random(10) . '.' . $file->getClientOriginalExtension();
        $path = $this->saveVideo($file, $filename,'upload/videos/news/');
        return $this->model->update([
            'video' => $path
        ]);
    }

    public function delete_news($news)
    {
       $listImg_repo = new ListImgNewsRepository(new ListImgNews);
       $del_list_img = $listImg_repo->delete_list($news->id);
       $del_new = $news->delete();

        //del image
        if(file_exists(public_path($news->image))){
            File::delete(public_path($news->image));
        }
        //del video
        if(file_exists(public_path($news->video))){
            File::delete(public_path($news->video));
        }
       //del list img
        $list_img = $listImg_repo->get_list_img($news->id);
        foreach($list_img as $img){
            if(file_exists(public_path($img->path))){
                File::delete(public_path($img->path));
            }
        } 
    }

    public function delete_list_news(array $list_id_news)
    {
        $repo_listimg = new ListImgNewsRepository((new ListImgNews));

        $list_news = $this->model->WhereIn('id',$list_id_news)->get();
        foreach($list_news as $new){
            $del_list_img = $repo_listimg->delete_list($new->id);
            
            $path_image = $new->image; 
            if(file_exists(public_path($path_image))){
                File::delete(public_path($path_image));
            }
        }

        return $this->model->whereIn('id',$list_id_news)->delete();
    }

}

?>
