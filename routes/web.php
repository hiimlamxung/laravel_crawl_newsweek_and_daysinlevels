<?php

use Illuminate\Support\Facades\Route;
use App\Models\Admins\Admin;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login.admin');
});
Route::group(['prefix' => 'backend', 'namespace' => 'backend'], function () {
    //auth
    Route::group(['namespace' => 'auth', 'middleware' => 'check.logged'], function () {
        Route::get('login', 'LoginController@index')->name('login.admin');

        Route::post('postlogin', 'LoginController@login')->name('login.admin.post');

        Route::get('register','RegisterController@index')->name('register.admin');
        Route::post('postregister','RegisterController@register')->name('register.admin.post');
    });

    Route::group(['middleware' => 'check.login'], function () {
        Route::get('home', function () {
            return view('admin.home');
        })->name('home.admin');


        //News
        Route::group(['prefix' => 'news'], function(){
            Route::match(['get','post'],'/manager', 'NewsController@managerNews')->name('news.manager')->middleware('permission:read-news,guard:admin');
            Route::get('/create', 'NewsController@createNews')->name('news.create')->middleware('permission:create-news,guard:admin');
            Route::post('/store', 'NewsController@storeNews')->name('news.store')->middleware('permission:create-news,guard:admin');
            Route::get('/edit/{id}', 'NewsController@editNews')->name('news.edit');
            Route::post('/update/{id?}', 'NewsController@updateNews')->name('news.update');
            Route::get('/delete/{id}', 'NewsController@deleteNews')->name('news.delete');

            //Ajax
            Route::get('deletevideo/{id?}','NewsController@delete_video')->name('admin.news.delvideo');
            Route::group(['middleware' => 'permission:action-news,guard:admin'],function(){
                Route::post('update_stt','NewsController@update_stt')->name('admin.news.updatestt');
                Route::post('PostedListNews','NewsController@PostedListNews')->name('admin.news.postedlist');
            });
            Route::post('DeleteListNews','NewsController@DeleteListNews')->name('admin.news.dellist')->middleware('permission:delete-list-news,guard:admin');


        });

        Route::group(['prefix' => 'listimgnews'], function(){
           Route::get('deleteimg/{id?}','ListImgNewsController@delete_img')->name('admin.news.delimg');
        });

        //admins
        Route::group(['prefix' => 'admins','middleware' => ['role:administrator,guard:admin']], function(){
            Route::get('/','AdminController@index')->name('admin.admins.index');

            Route::get('/create','AdminController@create')->name('admin.admins.create');
            Route::post('/store','AdminController@store')->name('admin.admins.store');

            Route::get('/edit/{id}','AdminController@edit')->name('admin.admins.edit');
            Route::post('/update/{id}','AdminController@update')->name('admin.admins.update');

            Route::get('/delete/{id}','AdminController@delete')->name('admin.admins.delete');

            Route::post('/activeorstop/{id?}','AdminController@activeorstop')->name('admin.admins.activeorstop');

            //ACL
            Route::get('/roles','AdminController@roles')->name('admin.admins.roles');
            Route::post('/roles/storerole','AdminController@store_role')->name('admin.admins.roles.storerole');
            Route::post('/roles/storepermission','AdminController@store_permission')->name('admin.admins.roles.storepermission');
            Route::post('/roles/addpermissiontorole','AdminController@addPermissionToRole')->name('admin.admins.roles.addpermissiontorole');
            Route::post('/delrole','AdminController@delete_role')->name('admin.admins.roles.delete');
            Route::post('/updaterole','AdminController@updateRoleAdmin')->name('admin.admins.roles.update');
            Route::post('/updatepermission','AdminController@updatePermissionAdmin')->name('admin.admins.permission.update');
            
         });

         //profile
         Route::group(['prefix' => 'profile'], function(){
            Route::get('/{admin}','ProfileController@profile')->name('admin.profile');
            Route::post('/update/{admin}','ProfileController@updateProfile')->name('admin.profile.update');

            Route::post('/updateImg/{admin}','ProfileController@updateImage')->name('admin.profile.updateimg');

         });

        //statistical
        Route::group(['prefix' => 'statistical','middleware' => 'permission:read-statistical,guard:admin'], function(){
            Route::get('/ctv','StatisticalController@ctv')->name('admin.statistical.ctv');
            Route::get('/censorship','StatisticalController@censorship')->name('admin.statistical.censorship');
         });
    });
});

//logout
Route::get('backend/logout', 'backend\auth\LoginController@logout')->name('logout.admin');

Route::get('test',function(){
    $fields = array(
        'to' => "/topics/easy-news",
        'notification'   => array(
            "title"  => "Day la noti test",
        ),
        'data' => array(
            'newsID' => 2,
            'total' => 3,
            'type' => "easy"
        )
    );
    
    return json_encode($fields);
});