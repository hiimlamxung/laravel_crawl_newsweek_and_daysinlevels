$(document).ready(function () {
    $('body').on('submit', '#frm_add_news', function (e) {
        e.preventDefault();
        var title = $("#title").val().trim();
        var link = $("#link").val().trim();
        var pub_date = $("#pub_date").val().trim();
        var image = $("#image")[0].files[0];
        var list_img = $("#list_img")[0].files;
        var video = $("#video")[0].files[0];
        var description = $("#description").val().trim();
        var content = $("#content").val().trim();
        var url = store;

        if (title == '' || title == null) {
            toastr.warning('Bạn chưa nhập tiêu đề');
            return false;
        }
        if (link == '' || link == null) {
            toastr.warning('Bạn chưa nhập link');
            return false;
        }
        if (pub_date == '' || pub_date == null) {
            toastr.warning('Bạn chưa nhập ngày');
            return false;
        }
        if (image == '' || image == null || image == 'undifine') {
            toastr.warning('Bạn chưa nhập ảnh đại diện');
            return false;
        }
        if (description == '' || description == null) {
            toastr.warning('Bạn chưa nhập mô tả');
            return false;
        }
        if (content == '' || content == null) {
            toastr.warning('Bạn chưa nhập nội dung');
            return false;
        }

        var formData = new FormData($("#frm_add_news")[0]);
        // var formData = new FormData();
        // formData.append('image', image);
        // formData.append('title', title);
        // formData.append('link', link);
        // formData.append('pub_date', pub_date);
        // console.log(formData);
        // ...

        $.ajax({
            type: "POST",
            url: url,
            data: formData,
            //Phải disable 2 option này
            processData: false, // tell jQuery not to process the data
            contentType: false,
            headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
            success: function (res) {
                toastr.success('Thêm báo thành công');
                window.location.reload();
            },
            error: function (data) {
                let error = $.parseJSON(data.responseText);
                toastr.warning(error);
            }
        });
    });


})
