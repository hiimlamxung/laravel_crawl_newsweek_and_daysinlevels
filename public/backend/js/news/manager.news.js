$(document).ready(function () {
    $('body').on('click', '.btn_del_new', function (e) {
        var check = confirm('Bạn có muốn xóa bài báo này không ?');
        if (!check) {
            e.preventDefault();
        }
    });

    //update stt
    $('body').on('change', '.stt_of_news', function (e) {
        var stt = $(this).val();
        var id = $(this).attr('data-id');
        var url = url_updatestt;
        var check = confirm('Bạn có muốn thay đổi trạng thái bài báo này không ?');
        if (check) {
            $.ajax({
                type: "POST",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')
                },
                data: {
                    stt: stt,
                    id: id
                },
                success: function (response) {
                    toastr.success('Thay đổi thành công');
                },
                error: function (response) {
                    toastr.warning('Thao tác thất bại');
                }
            });
        }
        $("#boxout_table_manager_news").load(" #boxin_table_manager_news");
    });

    //check all news
    $('body').on('click', '#check_all_news', function () {
        $(".check_news").prop('checked', this.checked);
    });

    //phát hành nhiều báo
    $('body').on('click', '#post_news', function () {
        var list_news = [];
        var url = url_posted_listnews;
        $(".check_news:checked").each(function () {
            list_news.push($(this).attr('data-id'));
        });

        if (list_news.length < 1) {
            alert('Chưa có bài báo nào được chọn !');
        } else {
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    list_news: list_news
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')
                },
                success: function (response) {
                    toastr.success('Phát hành báo thành công');
                    $("#boxout_table_manager_news").load(" #boxin_table_manager_news");
                },
                error: function (response) {
                    toastr.warning('Thao tác thất bại');
                }
            });
        }
    });

    //Xóa nhiều báo
    $('body').on('click', '#del_list_news', function () {
        var list_news = [];
        var url = url_delete_listnews;
        var check = confirm('Bạn có muốn xóa những bài báo đã chọn không ?');
        $(".check_news:checked").each(function () {
            list_news.push($(this).attr('data-id'));
        });

        if (list_news.length < 1) {
            alert('Chưa có bài báo nào được chọn !');
        } else {
            if (check) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        list_news: list_news
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')
                    },
                    success: function (response) {
                        toastr.success('Xóa thành công');
                        $("#boxout_table_manager_news").load(" #boxin_table_manager_news");
                    },
                    error: function (response) {
                        toastr.warning('Thao tác thất bại');
                    }
                });
            }
        }
    });


    //click button update stt
    $('body').on('click', '.btn_update_stt', function () {
        var stt = $(this).attr('data-stt');
        var id = $(this).attr('data-id');
        var url = url_updatestt;

        $.ajax({
            type: "POST",
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')
            },
            data: {
                id: id,
                stt: stt
            },
            success: function (response) {
                $("#boxout_table_manager_news").load(" #boxin_table_manager_news");
            },
            error: function (response) {
                toastr.warning('Thao tác thất bại');
            }
        });
    });

    //click detail content
    $('.detail-cont').click(function () {
        var content = $(this).data('content');
        // $('.modal-title').html('Nội dung');
        $('.modal-body').html(content);
        $('#modal-detail').modal('show');
    });
})
