$(document).ready(function () {
    $('body').on('submit', '#frm_edit_news', function (e) {
        e.preventDefault();
        var id = $("#news_id").val().trim();
        var title = $("#title").val().trim();
        var link = $("#link").val().trim();
        var pub_date = $("#pub_date").val().trim();
        var image = $("#image")[0].files[0];
        var list_img = $("#list_img")[0].files;
        var video = $("#video")[0].files[0];
        var status = $("#status").val().trim();
        var description = $("#description").val().trim();
        var content = $("#content").val().trim();
        var url = update;
        
        if (title == '' || title == null) {
            toastr.warning('Bạn chưa nhập tiêu đề');
            return false;
        }
        if (link == '' || link == null) {
            toastr.warning('Bạn chưa nhập link');
            return false;
        }
        if (pub_date == '' || pub_date == null) {
            toastr.warning('Bạn chưa nhập ngày');
            return false;
        }
        if (description == '' || description == null) {
            toastr.warning('Bạn chưa nhập mô tả');
            return false;
        }
        if (content == '' || content == null) {
            toastr.warning('Bạn chưa nhập nội dung');
            return false;
        }

        var formData = new FormData($("#frm_edit_news")[0]);

        $.ajax({
            type: "POST",
            url: url + "/" + id,
            data: formData,
            //Phải disable 2 option này
            processData: false, // tell jQuery not to process the data
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')
            },
            success: function (res) {
                toastr.success('Cập nhật báo thành công');
                // window.location.reload();
            },
            error: function (data) {
                let error = $.parseJSON(data.responseText);
                toastr.warning(error);
            }
        });
    });

    //del img in list
    $('body').on('click', '.btn_del_listimg', function (e) {
        e.preventDefault();
        var id_img = $(this).attr('data-id');
        var parent = $(this).parent();
        var url = delimg;
        var check = confirm('Bạn có muốn xóa ảnh này ?');
        if (check) {
            $.ajax({
                type: "GET",
                url: url + "/" + id_img,
                success: function (response) {
                    toastr.success('Xoá thành công');
                    parent.remove();
                },
                error: function (data) {
                    let error = $.parseJSON(data.responseText);
                    toastr.warning(error);
                }
            });
        }
    })

    //del video
    $('body').on('click', '.del_video_new', function (e) {
        e.preventDefault();
        var id_new = $(this).attr('data-id');
        var parent = $(this).parent();
        var url = delvideo;
        var check = confirm('Bạn có muốn xóa video cho bài báo này ?');
        if (check) {
            $.ajax({
                type: "GET",
                url: url + "/" + id_new,
                success: function (response) {
                    toastr.success('Xóa thành công');
                    parent.remove();
                },
                error: function (data) {
                    let error = $.parseJSON(data.responseText);
                    toastr.warning(error);
                }
            });
        }
    })

})
