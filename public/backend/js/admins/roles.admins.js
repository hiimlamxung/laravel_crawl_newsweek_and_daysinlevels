$(document).ready(function () {
    var data = roles.data;
    $("body").on('click', '.btn-add-per-to-role', function () {
        var id = $(this).data('id');
        var key = $(this).data('key');
        var role = data[key];
        var permissionsInRole = role.permissions.map(function (item) {
            return item.name;
        });
        $('input[type=checkbox]').each(function () {
            var per = $(this).attr('name');
            (permissionsInRole.indexOf(per) > -1) ? this.checked = true: this.checked = false;
        })
        $('.btn-save-change-permission').data('id', id);
        $('.bs-add-permission-role-modal-sm').modal('toggle');
    });

    // Save change permission
    $("body").on('click', '.btn-save-change-permission', function () {
        var id = $(this).data('id');
        var permissions = [];
        $('input[type=checkbox]').each(function () {
            if (this.checked) {
                permissions.push($(this).val());
            }
        });
        $.ajax({
            type: 'POST',
            url: link_update_permission_role,
            data: {
                id: id,
                permissions: permissions,
                _method: 'POST'
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')
            },
            success: function (res) {
                $('.bs-add-permission-role-modal-sm').modal('hide');
                toastr.success('Thêm thành công');
                setTimeout(() => {
                    window.location.reload();
                }, 1000);
            },
            error: function (e) {
                $('.bs-add-permission-role-modal-sm').modal('hide');
                toastr.warning('Thêm thất bại');
            }
        });
    });

    //del role
        // Delete role
        $('.btn-delete-role').click(function(e){
            if(confirm('Bạn có chắc muốn xoá quyền này?')){
                var id = $(this).data('id');
                $.ajax({
                    type: 'POST',
                    url: link_delete_role,
                    data: {
                        id: id,
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')
                    },
                    success:function(res){
                        toastr.success('Xóa thành công');
                        setTimeout(() => {
                            window.location.reload(true);
                        }, 1000);
                    },
                    error: function(e){
                        toastr.warning('Xóa thất bại');
                    }
                });
            }
        })
});
