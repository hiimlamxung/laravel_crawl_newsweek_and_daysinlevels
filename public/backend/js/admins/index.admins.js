
$(document).ready(function () {
    $('body').on('click', '.btn_del_admin', function (e) {
        var check = confirm('Xoá thành viên này sẽ đồng thời xóa hết bài báo của họ đã viết. Bạn đồng ý xóa không ?');
        if (!check) {
            e.preventDefault();
        }
    });

    $('body').on('click', '.btn_active', function (e) {
        var active = parseInt($(this).attr('data-active'));
        var id = $(this).attr('data-id');
        var this_element = $(this);
        var url = activeorstop+"/"+id;
        $.ajax({
            type: "POST",
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')
            },
            data: {
                active: active,
            },
            success: function (res) {
                if(active == 0){
                    this_element.html('Ngừng hoạt động');
                    this_element.attr('class','btn btn-danger btn-sm btn_active');
                    this_element.attr('data-active','1');
                }else{
                    this_element.html('Hoạt động');
                    this_element.attr('class','btn btn-success btn-sm btn_active');
                    this_element.attr('data-active','0');
                }
            },
        });
    });

    $('.btn-role-for-admin').click(function (e) {
        var id = $(this).data('id');
        var key = $(this).data('key');
        var account = data.data[key];
        var roles = account.roles.map(function (item) {
            return item.name;
        });
        $('.roles').each(function () {
            var role = $(this).attr('name');
            (roles.indexOf(role) > -1) ? this.checked = true: this.checked = false;
        })
        $('.btn-save-change-role-admin').data('id', id);
        $('.modal-role-admin').modal('toggle');
    })

    $('.btn-permission-for-admin').click(function (e) {
        var id = $(this).data('id');
        var key = $(this).data('key');
        var account = data.data[key];
        var permissions = account.permissions.map(function (item) {
            return item.name;
        });

        $('.permissions').each(function () {
            var permission = $(this).attr('name');
            (permissions.indexOf(permission) > -1) ? this.checked = true: this.checked = false;
        })
        $('.btn-save-change-permission').data('id', id);
        $('.modal-permission-admin').modal('toggle');
    })

    // Save change role for admin
    $('.btn-save-change-role-admin').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var roles = [];
        $('.roles').each(function () {
            if (this.checked) {
                roles.push($(this).val());
            }
        });
        $.ajax({
            type: 'POST',
            url: link_update_admin_role,
            data: {
                id: id,
                roles: roles,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')
            },
            success: function (res) {
                $('.modal-role-admin').modal('hide');
                toastr.success('Thêm thành công');
                setTimeout(() => {
                    window.location.reload(1)
                }, 1000);
            },
            error: function (e) {
                $('.modal-role-admin').modal('hide');
                toastr.warning('Thêm thất bại');
            }
        });
    })

    $('.btn-save-change-permission').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var permissions = [];
        $('.permissions').each(function () {
            if (this.checked) {
                permissions.push($(this).val());
            }
        });
        $.ajax({
            type: 'POST',
            url: link_update_admin_permission,
            data: {
                id: id,
                permissions: permissions,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')
            },
            success: function (res) {
                $('.modal-permission-admin').modal('hide');
                toastr.success('Thêm thành công');
                setTimeout(() => {
                    window.location.reload(1)
                }, 1000);
            },
            error: function (e) {
                $('.modal-permission-admin').modal('hide');
                toastr.warning('Thêm thất bại');
            }
        });
    })
})
